
/* Includes ------------------------------------------------------------------*/

//#include "utils.h"
#include "tft.h"

#define _dc_low() TFT_DC_LOW
#define _dc_high() TFT_DC_HIGH
#define _cs_low() TFT_CS_LOW
#define _cs_high() TFT_CS_HIGH

#define tft_spiwrite16(data) \
    TFT_sendByte(data >> 8); \
    TFT_sendByte(data & 0x00ff);

void tft_fillRect(short x, short y, short w, short h, unsigned short color);

short _width = ILI9340_TFTWIDTH;
short _height = ILI9340_TFTHEIGHT;

#define SPI_NSSInternalSoft_Set SPI_CR1_SSI
#define SPI_NSSInternalSoft_Reset ((uint16_t)0xFEFF)
#define IS_SPI_NSS_INTERNAL(INTERNAL) (((INTERNAL) == SPI_NSSInternalSoft_Set) || ((INTERNAL) == SPI_NSSInternalSoft_Reset))

#define SPI_I2S_FLAG_RXNE SPI_SR_RXNE
#define SPI_I2S_FLAG_TXE SPI_SR_TXE
#define I2S_FLAG_CHSIDE SPI_SR_CHSIDE
#define I2S_FLAG_UDR SPI_SR_UDR
#define SPI_FLAG_CRCERR SPI_SR_CRCERR
#define SPI_FLAG_MODF SPI_SR_MODF
#define SPI_I2S_FLAG_OVR SPI_SR_OVR
#define SPI_I2S_FLAG_BSY SPI_SR_BSY
#define SPI_I2S_FLAG_FRE SPI_SR_FRE

typedef enum {
    Bit_RESET = 0,
    Bit_SET
} BitAction;


void SPI_NSSInternalSoftwareConfig(SPI_TypeDef* SPIx, uint16_t SPI_NSSInternalSoft)
{
    /* Check the parameters */
    assert_param(IS_SPI_ALL_PERIPH(SPIx));
    assert_param(IS_SPI_NSS_INTERNAL(SPI_NSSInternalSoft));

    if (SPI_NSSInternalSoft != SPI_NSSInternalSoft_Reset) {
        /* Set NSS pin internally by software */
        SPIx->CR1 |= SPI_NSSInternalSoft_Set;
    }
    else {
        /* Reset NSS pin internally by software */
        SPIx->CR1 &= SPI_NSSInternalSoft_Reset;
    }
}

void SPI_Cmd(SPI_TypeDef* SPIx, FunctionalState NewState)
{
    /* Check the parameters */
    assert_param(IS_SPI_ALL_PERIPH(SPIx));
    assert_param(IS_FUNCTIONAL_STATE(NewState));

    if (NewState != DISABLE) {
        /* Enable the selected SPI peripheral */
        SPIx->CR1 |= SPI_CR1_SPE;
    }
    else {
        /* Disable the selected SPI peripheral */
        SPIx->CR1 &= (uint16_t) ~((uint16_t)SPI_CR1_SPE);
    }
}

uint8_t SPI_ReceiveData8(SPI_TypeDef* SPIx)
{
    uint32_t spixbase = 0x00;

    spixbase = (uint32_t)SPIx;
    spixbase += 0x0C;

    return *(__IO uint8_t*)spixbase;
}

void SPI_SendData8(SPI_TypeDef* SPIx, uint8_t Data)
{
    uint32_t spixbase = 0x00;

    /* Check the parameters */
    assert_param(IS_SPI_ALL_PERIPH(SPIx));

    spixbase = (uint32_t)SPIx;
    spixbase += 0x0C;

    *(__IO uint8_t*)spixbase = Data;
}

FlagStatus SPI_I2S_GetFlagStatus(SPI_TypeDef* SPIx, uint16_t SPI_I2S_FLAG)
{
    FlagStatus bitstatus = RESET;
    /* Check the parameters */
    assert_param(IS_SPI_ALL_PERIPH(SPIx));
    assert_param(IS_SPI_I2S_GET_FLAG(SPI_I2S_FLAG));

    /* Check the status of the specified SPI flag */
    if ((SPIx->SR & SPI_I2S_FLAG) != (uint16_t)RESET) {
        /* SPI_I2S_FLAG is set */
        bitstatus = SET;
    }
    else {
        /* SPI_I2S_FLAG is reset */
        bitstatus = RESET;
    }
    /* Return the SPI_I2S_FLAG status */
    return bitstatus;
}

void TFT_sendByte(uint8_t data)
{
    //while(SPI_I2S_GetFlagStatus(TFT_SPI, SPI_I2S_FLAG_TXE)==RESET);
    SPI_SendData8(TFT_SPI, data);
    //for (int i = 0; i < 5;i++);
    asm ("nop");
}

void TFT_sendCMD(int index)
{
    // ���� ����� ������ ������ ���� �� ���� ��� �� �������� ��������� ����� ���������� ��������
    while(SPI_I2S_GetFlagStatus(TFT_SPI, SPI_I2S_FLAG_BSY) == SET);
    TFT_CS_HIGH;
    TFT_DC_LOW;
    TFT_CS_LOW;

    TFT_sendByte(index);
}

void TFT_sendDATA(int data)
{
    while (SPI_I2S_GetFlagStatus(TFT_SPI, SPI_I2S_FLAG_BSY) == SET)
        ;
    TFT_DC_HIGH;
    TFT_sendByte(data);
}

void TFT_sendWord(int data)
{
    while (SPI_I2S_GetFlagStatus(TFT_SPI, SPI_I2S_FLAG_BSY) == SET)
        ;
    TFT_DC_HIGH;

    TFT_sendByte(data >> 8);
    TFT_sendByte(data & 0x00ff);
}

int TFT_Read_Register(int Addr, int xParameter)
{
    int data = 0;

    TFT_sendCMD(0xD9); // ext command
    TFT_sendByte(0x10 + xParameter); // 0x11 is the first Parameter
    TFT_DC_LOW;
    TFT_CS_LOW;
    TFT_sendByte(Addr);
    TFT_DC_HIGH;
    data = SPI_ReceiveData8(TFT_SPI);
    TFT_CS_HIGH;

    return data;
}

int TFT_readID(void)
{
    int i = 0;
    int byte;
    int data[3] = { 0x00, 0x00, 0x00 };
    int ID[3] = { 0x00, 0x93, 0x41 };
    int ToF = 1;

    for (i = 0; i < 3; i++) {
        byte = TFT_Read_Register(0xD3, i + 1);
        data[i] = byte;

        if (data[i] != ID[i]) {
            ToF = 0;
        }
    }

    return ToF;
}

void TFT_init()
{
    int i;
    /*	GPIO_InitTypeDef gpio;

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

	gpio.GPIO_Pin = PIN_LED | PIN_DC | PIN_RST | PIN_CS;
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	gpio.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_Init(TFT_PORT, &gpio);

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);

	gpio.GPIO_Pin = PIN_SCK | PIN_MOSI;
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	gpio.GPIO_Mode = GPIO_Mode_AF;
  GPIO_Init(TFT_PORT, &gpio);

  gpio.GPIO_Pin = PIN_MISO;
	gpio.GPIO_Mode = GPIO_Mode_IN;
    GPIO_Init(TFT_PORT, &gpio);

    //SPI_StructInit(&spi);
	SPI_InitTypeDef spi;
	spi.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	spi.SPI_Mode = SPI_Mode_Master;
	spi.SPI_DataSize = SPI_DataSize_8b;
	spi.SPI_CPOL = SPI_CPOL_Low;
	spi.SPI_CPHA = SPI_CPHA_1Edge;
	spi.SPI_NSS = SPI_NSS_Soft;
	spi.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_4;
	spi.SPI_FirstBit = SPI_FirstBit_MSB;
	SPI_Init(TFT_SPI, &spi);*/

//    TFT_SPI->CR1 = 0x0000034C;
//    TFT_SPI->CR2 = 0x00000700;

    TFT_SPI->CR1 &= ~SPI_CR1_BIDIMODE;
    TFT_SPI->CR1 |= SPI_CR1_SPE;
    TFT_RST_LOW;
    for (i = 0; i < 0x0FFFF; i++)
        ;
    TFT_RST_HIGH;

    TFT_sendCMD(0xEF);
    TFT_sendDATA(0x03);
    TFT_sendDATA(0x80);
    TFT_sendDATA(0x02);

    TFT_sendCMD(0xCF);
    TFT_sendDATA(0x00);
    TFT_sendDATA(0XC1);
    TFT_sendDATA(0X30);

    TFT_sendCMD(0xED);
    TFT_sendDATA(0x64);
    TFT_sendDATA(0x03);
    TFT_sendDATA(0X12);
    TFT_sendDATA(0X81);

    TFT_sendCMD(0xE8);
    TFT_sendDATA(0x85);
    TFT_sendDATA(0x00);
    TFT_sendDATA(0x78);

    TFT_sendCMD(0xCB);
    TFT_sendDATA(0x39);
    TFT_sendDATA(0x2C);
    TFT_sendDATA(0x00);
    TFT_sendDATA(0x34);
    TFT_sendDATA(0x02);

    TFT_sendCMD(0xF7);
    TFT_sendDATA(0x20);

    TFT_sendCMD(0xEA);
    TFT_sendDATA(0x00);
    TFT_sendDATA(0x00);

    TFT_sendCMD(ILI9340_PWCTR1); //Power control
    TFT_sendDATA(0x23); //VRH[5:0]

    TFT_sendCMD(ILI9340_PWCTR2); //Power control
    TFT_sendDATA(0x10); //SAP[2:0];BT[3:0]

    TFT_sendCMD(ILI9340_VMCTR1); //VCM control
    TFT_sendDATA(0x3e); //�Աȶȵ���
    TFT_sendDATA(0x28);

    TFT_sendCMD(ILI9340_VMCTR2); //VCM control2
    TFT_sendDATA(0x86); //--

    TFT_sendCMD(ILI9340_MADCTL); // Memory Access Control
    TFT_sendDATA(0x48);

    TFT_sendCMD(ILI9340_PIXFMT);
    TFT_sendDATA(0x55);

    TFT_sendCMD(ILI9340_FRMCTR1);
    TFT_sendDATA(0x00);
    TFT_sendDATA(0x18);

    TFT_sendCMD(ILI9340_DFUNCTR); // Display Function Control
    TFT_sendDATA(0x08);
    TFT_sendDATA(0x82);
    TFT_sendDATA(0x27);

    TFT_sendCMD(0xF2); // 3Gamma Function Disable
    TFT_sendDATA(0x00);

    TFT_sendCMD(ILI9340_GAMMASET); //Gamma curve selected
    TFT_sendDATA(0x01);

    TFT_sendCMD(ILI9340_GMCTRP1); //Set Gamma
    TFT_sendDATA(0x0F);
    TFT_sendDATA(0x31);
    TFT_sendDATA(0x2B);
    TFT_sendDATA(0x0C);
    TFT_sendDATA(0x0E);
    TFT_sendDATA(0x08);
    TFT_sendDATA(0x4E);
    TFT_sendDATA(0xF1);
    TFT_sendDATA(0x37);
    TFT_sendDATA(0x07);
    TFT_sendDATA(0x10);
    TFT_sendDATA(0x03);
    TFT_sendDATA(0x0E);
    TFT_sendDATA(0x09);
    TFT_sendDATA(0x00);

    TFT_sendCMD(ILI9340_GMCTRN1); //Set Gamma
    TFT_sendDATA(0x00);
    TFT_sendDATA(0x0E);
    TFT_sendDATA(0x14);
    TFT_sendDATA(0x03);
    TFT_sendDATA(0x11);
    TFT_sendDATA(0x07);
    TFT_sendDATA(0x31);
    TFT_sendDATA(0xC1);
    TFT_sendDATA(0x48);
    TFT_sendDATA(0x08);
    TFT_sendDATA(0x0F);
    TFT_sendDATA(0x0C);
    TFT_sendDATA(0x31);
    TFT_sendDATA(0x36);
    TFT_sendDATA(0x0F);

    TFT_sendCMD(ILI9340_SLPOUT); //Exit Sleep
    for (i = 0; i < 0x00FFF; i++)
        ;
    TFT_sendCMD(ILI9340_DISPON); //Display on
}

void TFT_led(int state)
{
   // if (state)
  //      HAL_GPIO_WritePin(TFT_LED_GPIO_Port, TFT_LED_Pin, GPIO_PIN_SET);
    //else
  //      HAL_GPIO_WritePin(TFT_LED_GPIO_Port, TFT_LED_Pin, GPIO_PIN_RESET);
}

void TFT_setCol(int StartCol, int EndCol)
{
    TFT_sendCMD(0x2A); // Column Command address
    TFT_sendWord(StartCol);
    TFT_sendWord(EndCol);
}

void TFT_setPage(int StartPage, int EndPage)
{
    TFT_sendCMD(0x2B); // Column Command address
    TFT_sendWord(StartPage);
    TFT_sendWord(EndPage);
}

void TFT_setXY(int poX, int poY)
{
    TFT_setCol(poX, poX);
    TFT_setPage(poY, poY);
    TFT_sendCMD(0x2c);
}

void TFT_setPixel(int poX, int poY, int color)
{
    TFT_setXY(poX, poY);
    TFT_sendWord(color);
}

void fillScreen(void)
{
    long i = 0;
    TFT_setCol(0, 239);
    TFT_setPage(0, 319);
    TFT_sendCMD(0x2c); /* start to write to display ra */
    /* m                            */

    TFT_DC_HIGH;
    TFT_CS_LOW;
    for (i = 0; i < 38400; i++) {
        TFT_sendByte(0);
        TFT_sendByte(0);
        TFT_sendByte(0);
        TFT_sendByte(0);
    }
    TFT_CS_HIGH;
}

void tft_setAddrWindow(unsigned short x0, unsigned short y0, unsigned short x1, unsigned short y1)
{

    TFT_sendCMD(ILI9340_CASET); // Column addr set
    TFT_sendWord(x0);
    TFT_sendWord(x1);

    TFT_sendCMD(ILI9340_PASET); // Row addr set
    TFT_sendWord(y0);
    TFT_sendWord(y1);

    TFT_sendCMD(ILI9340_RAMWR); // write to RAM
}

void tft_pushColor(unsigned short color)
{
    _dc_high();
    _cs_low();

    TFT_sendByte(color >> 8);
    TFT_sendByte(color & 0x00ff);

    _cs_high();
}
#define NOP asm("nop");
#define wait16 \
    NOP;       \
    NOP;       \
    NOP;       \
    NOP;       \
    NOP;       \
    NOP;       \
    NOP;       \
    NOP;       \
    NOP;       \
    NOP;       \
    NOP;       \
    NOP;       \
    NOP;       \
    NOP;       \
    NOP;       \
    NOP;
#define wait8 \
    NOP;      \
    NOP;      \
    NOP;      \
    NOP;      \
    NOP;      \
    NOP;      \
    NOP;      \
    NOP;
#define wait12 \
    NOP;       \
    NOP;       \
    NOP;       \
    NOP;       \
    NOP;       \
    NOP;       \
    NOP;       \
    NOP;       \
    NOP;       \
    NOP;       \
    NOP;       \
    NOP;
#define wait15 \
    NOP;       \
    NOP;       \
    NOP;       \
    NOP;       \
    NOP;       \
    NOP;       \
    NOP;       \
    NOP;       \
    NOP;       \
    NOP;       \
    NOP;       \
    NOP;       \
    NOP;       \
    NOP;       \
    NOP;

void tft_drawPixel(short x, short y, unsigned short color)
{
    /* Draw a pixel at location (x,y) with given color
 * Parameters:
 *      x:  x-coordinate of pixel to draw; top left of screen is x=0
 *              and x increases to the right
 *      y:  y-coordinate of pixel to draw; top left of screen is y=0
 *              and y increases to the bottom
 *      color:  16-bit color value
 * Returns:     Nothing
 */

    if ((x < 0) || (x >= _width) || (y < 0) || (y >= _height))
        return;

    TFT_setPixel(x, y, color);
}

void tft_drawFastVLine(short x, short y, short h, unsigned short color)
{
    /* Draw a vertical line at location from (x,y) to (x,y+h-1) with color
 * Parameters:
 *      x:  x-coordinate line to draw; top left of screen is x=0
 *              and x increases to the right
 *      y:  y-coordinate of starting point of line; top left of screen is y=0
 *              and y increases to the bottom
 *      h:  height of line to draw
 *      color:  16-bit color value
 * Returns:     Nothing
 */

    // Rudimentary clipping
    if ((x >= _width) || (y >= _height))
        return;

    if ((y + h - 1) >= _height)
        h = _height - y;

    tft_setAddrWindow(x, y, x, y + h - 1);

    _dc_high();
    _cs_low();

    while (h--) {
        tft_spiwrite16(color);
    }

    _cs_high();
}

void tft_drawFastHLine(short x, short y, short w, unsigned short color)
{
    /* Draw a horizontal line at location from (x,y) to (x+w-1,y) with color
 * Parameters:
 *      x:  x-coordinate starting point of line; top left of screen is x=0
 *              and x increases to the right
 *      y:  y-coordinate of starting point of line; top left of screen is y=0
 *              and y increases to the bottom
 *      w:  width of line to draw
 *      color:  16-bit color value
 * Returns:     Nothing
 */

    // Rudimentary clipping
    if ((x >= _width) || (y >= _height))
        return;
    if ((x + w - 1) >= _width)
        w = _width - x;
    tft_setAddrWindow(x, y, x + w - 1, y);

    _dc_high();
    _cs_low();

    while (w--) {
        tft_spiwrite16(color);
    }

    _cs_high();
}

void tft_fillScreen(unsigned short color)
{
    /* Fill entire screen with given color
 * Parameters:
 *      color: 16-bit color value
 * Returs:  Nothing
 */
    tft_fillRect(0, 0, _width, _height, color);
}

// fill a rectangle
void tft_fillRect(short x, short y, short w, short h,
    unsigned short color)
{
    /* Draw a filled rectangle with starting top-left vertex (x,y),
 *  width w and height h with given color
 * Parameters:
 *      x:  x-coordinate of top-left vertex; top left of screen is x=0
 *              and x increases to the right
 *      y:  y-coordinate of top-left vertex; top left of screen is y=0
 *              and y increases to the bottom
 *      w:  width of rectangle
 *      h:  height of rectangle
 *      color:  16-bit color value
 * Returns:     Nothing
 */

    // rudimentary clipping (drawChar w/big text requires this)
    if ((x >= _width) || (y >= _height))
        return;
    if ((x + w - 1) >= _width)
        w = _width - x;
    if ((y + h - 1) >= _height)
        h = _height - y;

    tft_setAddrWindow(x, y, x + w - 1, y + h - 1);

    _dc_high();
    _cs_low();

    for (y = h; y > 0; y--) {
        for (x = w; x > 0; x--) {
            tft_spiwrite16(color);
        }
    }

    _cs_high();
}

inline unsigned short tft_Color565(unsigned char r, unsigned char g, unsigned char b)
{
    /* Pass 8-bit (each) R,G,B, get back 16-bit packed color
 * Parameters:
 *      r:  8-bit R/red value from RGB
 *      g:  8-bit g/green value from RGB
 *      b:  8-bit b/blue value from RGB
 * Returns:
 *      16-bit packed color value for color info
 */
    return ((r & 0xF8) << 8) | ((g & 0xFC) << 3) | (b >> 3);
}

void tft_setRotation(unsigned char m)
{
    unsigned char rotation;
    TFT_sendCMD(ILI9340_MADCTL);
    rotation = m % 4; // can't be higher than 3
    switch (rotation) {
    case 0:
        TFT_sendDATA(/*ILI9340_MADCTL_MX | */ ILI9340_MADCTL_BGR);
        _width = ILI9340_TFTWIDTH;
        _height = ILI9340_TFTHEIGHT;
        break;
    case 1:
        TFT_sendDATA(ILI9340_MADCTL_MV | ILI9340_MADCTL_BGR);
        _width = ILI9340_TFTHEIGHT;
        _height = ILI9340_TFTWIDTH;
        break;
    case 2:
        TFT_sendDATA(ILI9340_MADCTL_MY | ILI9340_MADCTL_BGR);
        _width = ILI9340_TFTWIDTH;
        _height = ILI9340_TFTHEIGHT;
        break;
    case 3:
        TFT_sendDATA(ILI9340_MADCTL_MV | ILI9340_MADCTL_MY | ILI9340_MADCTL_MX | ILI9340_MADCTL_BGR);
        _width = ILI9340_TFTHEIGHT;
        _height = ILI9340_TFTWIDTH;
        break;
    }
}
void tft_invertDisplay(unsigned char i)
{
    TFT_sendCMD(i ? ILI9340_INVON : ILI9340_INVOFF);
}

void delay_ms(unsigned long i)
{
    /* Create a software delay about i ms long
 * Parameters:
 *      i:  equal to number of milliseconds for delay
 * Returns: Nothing
 * Note: Uses Core Timer. Core Timer is cleared at the initialiazion of
 *      this function. So, applications sensitive to the Core Timer are going
 *      to be affected
 */ /*
    unsigned int j;
    j = dTime_ms * i;
    WriteCoreTimer(0);
    while (ReadCoreTimer() < j);
    */
    HAL_Delay(i);
}

void delay_us(unsigned long i)
{
    /* Create a software delay about i us long
 * Parameters:
 *      i:  equal to number of microseconds for delay
 * Returns: Nothing
 * Note: Uses Core Timer. Core Timer is cleared at the initialiazion of
 *      this function. So, applications sensitive to the Core Timer are going
 *      to be affected
 */ /*
    unsigned int j;
    j = dTime_us * i;
    WriteCoreTimer(0);
    while (ReadCoreTimer() < j);
    */
    HAL_Delay(1);
}

//void tft_invertDisplay(boolean i) {
//  writecommand(i ? ILI9340_INVON : ILI9340_INVOFF);
//}

////////// stuff not actively being used, but kept for posterity

//unsigned char tft_spiread(void) {
//  unsigned char r = 0;
//
//  /*
//   * ADD SPI INTERFACE CODE -----------------------------------------------&**************************
//  */
//  //Serial.print("read: 0x"); Serial.print(r, HEX);
//
//  return r;
//}

// unsigned char tft_readdata(void) {
//   unsigned char r;
//   _dc_high();
//   _cs_low();
//   r = tft_spiread();
//   _cs_high();
//   return r;
//
//}
//
//
// unsigned char tft_readcommand8(unsigned char c) {
//     _dc_low();
////     _sclk = 0;
//     _cs_low();
//     tft_spiwrite8(c);
//
//     _dc_high();
//     unsigned char r = tft_spiread();
//     _cs_high();
//     return r;
//
//   /*
//   digitalWrite(_dc, LOW);
//   digitalWrite(_sclk, LOW);
//   digitalWrite(_cs, LOW);
//   spiwrite(c);
//
//   digitalWrite(_dc, HIGH);
//   unsigned char r = spiread();
//   digitalWrite(_cs, HIGH);
//   return r;
//    */
//}

void tft_setRotation(unsigned char m);
void tft_drawLine(short x0, short y0,
    short x1, short y1,
    unsigned short color);

void draw_vertical(short y0, short y1, short x)
{
    tft_drawLine(x, y0, x, y1, ILI9340_GRAY1);
}
void draw_horizontal(short x0, short x1, short y)
{
    tft_drawLine(x0, y, x1, y, ILI9340_GRAY1);
}
void draw_grid()
{
    for (int k = 0; k < 5; k++) {
        for (int j = 0; j < 5; j++) {
            tft_drawPixel(ILI9340_TFTWIDTH * j / 5, ILI9340_TFTHEIGHT * k / 5, ILI9340_GRAY1);
        }
    }
}

void draw_graph(uint8_t* p, uint16_t len, uint16_t color)
{
    for (int k = 0; k < len - 1; k++) {
        tft_drawLine(k, p[k], k + 1, p[k + 1], color);
    }
}
