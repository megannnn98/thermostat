#define PIN_OUT 12
#define PIN_IN  11


void setup() {
  // put your setup code here, to run once:
    pinMode(PIN_OUT, OUTPUT);
    pinMode(PIN_IN, INPUT_PULLUP);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(PIN_OUT, digitalRead(PIN_IN)); 
}
