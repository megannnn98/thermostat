#include "contiki.h"
#include "common_types.h"
#include "main.h"
#include "stm32f4xx_hal.h"
#include "printf.h"
#include "DS18B20.h"
#include <sys/stimer.h>

extern uint32_t relay_time_stamp, time_last_switch;
extern fix4 T_target;
extern relay_state_t relay_state;
extern fix4 f, f_last_switch, f_max, f_min, rate, mean, sigma;
/*---------------------------------------------------------------------------*/
PROCESS(relay_process, "Relay");

/*---------------------------------------------------------------------------*/
PROCESS_THREAD(relay_process, ev, data)
{

    static struct stimer st;
    static fix4 diff;
    static u32 t32;
//    if (sWAIT != relay_state) {
//        RELAY_ON;
//        PROCESS_EXIT();
//    }
    PROCESS_BEGIN();

    PROCESS_WAIT_EVENT();

    while (1) {
        
        // ����� �� ����� ����� ��������
        relay_time_stamp = clock_seconds();
//        PT_WAIT_UNTIL(pt, (HAL_GetTick() - relay_time_stamp) > (10 * MS_IN_MIN - 1));
        
        stimer_set(&st, 10*S_IN_MIN);
        PROCESS_WAIT_EVENT_UNTIL(stimer_expired(&st));

        //            sprintf(buffer,"%s", "relay on");
        //            HAL_UART_Transmit(&huart1, (uint8_t*)buffer, strlen(buffer), 1000);

        // ����� ��������

        // ����������� ������ - ���� ��������
        if (mean > T_target) {
          
            // � ���� ��������� - �� ��������� - ������� � ���� ��� 10 �����
            if (sRELAY_ON != relay_state) {
                RELAY_ON;
                //tft_drawCircle(ring_get_cnt(&tft_rms), f_to_tft(f), 3, ILI9340_RED);
            }
            // � ���� �������� - ���� ����� ������� �� ������ �����������
            else {
                if (sWAIT != relay_state) {
                    PROCESS_WAIT_WHILE(mean > T_target);
                }
                RELAY_OFF;
                //tft_drawCircle(ring_get_cnt(&tft_rms), f_to_tft(f), 3, ILI9340_GREEN);
            }
        }
        // ����������� ����� - ���� ���������
        else {
            // � ���� ��������� - ����������� - ����� ��������� �� ������ �����������
            if (sRELAY_ON != relay_state) {
                if (sWAIT != relay_state) {
                    PROCESS_WAIT_WHILE(mean < T_target);
                }
                RELAY_ON;
                //tft_drawCircle(ring_get_cnt(&tft_rms), f_to_tft(f), 3, ILI9340_RED);
            }
            // ���� �������� - ���������
            else {
                RELAY_OFF;
                //tft_drawCircle(ring_get_cnt(&tft_rms), f_to_tft(f), 3, ILI9340_GREEN);
            }
        }

        // ��������� ���������� �����������
        // ������� ������ �������
        diff = absfix4(f - f_last_switch);
        t32 = clock_seconds() - time_last_switch;
        rate = float2fix4((fix2float4(diff) / (t32)) * 60. * 60.);
        time_last_switch = t32;
        f_last_switch = f;
        
    }

    PROCESS_END();
}

