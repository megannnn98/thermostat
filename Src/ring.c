

/**
  ******************************************************************************
  * @file    circular_uart_utils.c
  * @author  Karatanov
  * @version V1.0.0
  * @date    05-February-2016
  * @brief   Circular buff functions 
  */

/* Includes ------------------------------------------------------------------*/
#include "ring.h"

/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Global functions ---------------------------------------------------------*/

/** @addtogroup UART
  * @{
  */

ring_t tft_rms, tft_means;
/** @addtogroup UART_Circular_Handlers
  * @{
  */

/**
  * @brief  �������� ������ � �����
  * @param  ����� UART ����������
  * @param  ����� ������ ������
  * @retval None
  */
void ring_put_char(ring_t* buf, const u16 sym)
{

    if (buf->cnt < RING_SIZE) { //���� � ������ ��� ���� �����
        buf->ring_array[buf->tail++] = sym; //�������� � ���� ������
        buf->cnt++; //�������������� ������� ��������
        if (buf->tail == RING_SIZE) {
            buf->tail = 0;
        }
    }
    else {
        ring_get_char(buf);
        ring_put_char(buf, sym);
    }
}

/**
  * @brief  ����� ������ �� ������
  * @param  ����� UART ����������
  * @retval ������ �� �������__get_interrupt_state__get_interrupt_state
  */
u16 ring_get_char(ring_t* buf)
{
    u16 sym = 0;

    __disable_irq();
    if (buf->cnt > 0) { //���� ����� �� ������
        sym = buf->ring_array[buf->head++]; //��������� ������ �� ������
        buf->cnt--; //��������� ������� ��������
        if (buf->head == RING_SIZE) {
            buf->head = 0;
        }
    }
    __enable_irq();
    return sym;
}

/**
  * @brief  
  * @param  
  * @retval 
  */
u16 ring_peek_char(ring_t* buf, u16 shift)
{
    u16 sym = 0;

    __disable_irq();
    if (buf->cnt > 0) { //���� ����� �� ������
        if (buf->head + shift >= RING_SIZE) {
            shift = (buf->head + shift) - RING_SIZE;
            sym = buf->ring_array[shift]; //��������� ������ �� ������
        }
        else {
            sym = buf->ring_array[buf->head + shift]; //��������� ������ �� ������
        }
    }
    __enable_irq();
    return sym;
}

/**
  * @brief  
  * @param  
  * @retval 
  */
u16 ring_get_head(ring_t* buf)
{
    return buf->head;
}

/**
  * @brief  
  * @param  
  * @retval 
  */
u16 ring_get_cnt(ring_t* buf)
{
    return buf->cnt;
}
/**
  * @brief  �������� ���� ������
  * @param  ����� UART ����������
  * @param  ��������� �� ������ � ������� ��������� 
  * @retval None
  */
void ring_readout(ring_t* buf, u8 p[])
{
    //static __istate_t s;
    //s = __get_interrupt_state();
    __disable_irq();
    for (u16 i = 0; buf->cnt; i++) {
        p[i] = ring_get_char(buf);
    }
    __enable_irq();
}

/**
  * @brief  �������" �����
  * @param  ����� UART ����������
  * @retval None
  */
void ring_flush(ring_t* buf)
{
    buf->tail = 0;
    buf->head = 0;
    buf->cnt = 0;
}

// --------------------------------------------------------------------------
