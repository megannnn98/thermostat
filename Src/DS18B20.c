

#include "stm32f4xx_hal.h"
#include "common_types.h"
#include "DS18B20.h"

void ONEWIRE_DELAY_2()
{
    uint16_t time_us = 5*2;

    while (time_us--) {
        while (TIM_SR_UIF != (_DS18B20_TIMER->SR & TIM_SR_UIF))
            ;
    }
}

void ONEWIRE_DELAY_5()
{
    uint16_t time_us = 5*7;

    while (time_us--) {
        while (TIM_SR_UIF != (_DS18B20_TIMER->SR & TIM_SR_UIF))
            ;
    }
}

void ONEWIRE_DELAY_10()
{
    uint16_t time_us = 5*14;

    while (time_us--) {
        while (TIM_SR_UIF != (_DS18B20_TIMER->SR & TIM_SR_UIF))
            ;
    }
}

void ONEWIRE_DELAY_15()
{
    uint16_t time_us = 5*21;

    while (time_us--) {
        while (TIM_SR_UIF != (_DS18B20_TIMER->SR & TIM_SR_UIF))
            ;
    }
}
void ONEWIRE_DELAY_20()
{
    ONEWIRE_DELAY_10();
    ONEWIRE_DELAY_10();
}

void ONEWIRE_DELAY_60()
{
    uint16_t time_us = 5*90;

    while (time_us--) {
        while (TIM_SR_UIF != (_DS18B20_TIMER->SR & TIM_SR_UIF))
            ;
    }
}

void ONEWIRE_DELAY_100()
{
    uint16_t time_us = 5*155;

    while (time_us--) {
        while (TIM_SR_UIF != (_DS18B20_TIMER->SR & TIM_SR_UIF))
            ;
    }
}

void ONEWIRE_DELAY_480()
{
    uint16_t time_us = 5*720;

    while (time_us--) {
        while (TIM_SR_UIF != (_DS18B20_TIMER->SR & TIM_SR_UIF))
            ;
    }
}

void ONEWIRE_LOW(OneWire_t* gp)
{
    gp->GPIOx->BSRR = gp->GPIO_Pin << 16;
}
void ONEWIRE_HIGH(OneWire_t* gp)
{
    gp->GPIOx->BSRR = gp->GPIO_Pin;
}
void ONEWIRE_INPUT(OneWire_t* gp)
{
    GPIO_InitTypeDef gpinit;
    gpinit.Mode = GPIO_MODE_INPUT;
    gpinit.Pull = GPIO_NOPULL;
    gpinit.Speed = GPIO_SPEED_FREQ_MEDIUM;
    gpinit.Pin = gp->GPIO_Pin;
    HAL_GPIO_Init(gp->GPIOx, &gpinit);
}
void ONEWIRE_OUTPUT(OneWire_t* gp)
{
    GPIO_InitTypeDef gpinit;
    gpinit.Mode = GPIO_MODE_OUTPUT_OD;
    gpinit.Pull = GPIO_NOPULL;
    gpinit.Speed = GPIO_SPEED_FREQ_MEDIUM;
    gpinit.Pin = gp->GPIO_Pin;
    HAL_GPIO_Init(gp->GPIOx, &gpinit);
}

#include "tft.h"
#include "math.h"

#define M_PI 3.14159265358979323846


void funny_delay(int x, int y, int r, float start_angle, float end_angle, int N, int delay) 
{
    for (float i = start_angle; i < end_angle; i = i + 0.05)
    {
        osDelay(delay/N);
        tft_drawPixel(x + (int)(cos(i) * (r-0)), 
                      y + (int)(sin(i) * (r-0)), 
                      ILI9340_WHITE); 
        tft_drawPixel(x + (int)(cos(i) * (r-1)), 
                      y + (int)(sin(i) * (r-1)), 
                      ILI9340_YELLOW); 
        tft_drawPixel(x + (int)(cos(i) * (r-2)), 
                      y + (int)(sin(i) * (r-2)), 
                      ILI9340_GREEN); 
        tft_drawPixel(x + (int)(cos(i) * (r-3)), 
                      y + (int)(sin(i) * (r-3)), 
                      ILI9340_RED); 
        tft_drawPixel(x + (int)(cos(i) * (r-4)), 
                      y + (int)(sin(i) * (r-4)), 
                      ILI9340_MAGENTA); 
        tft_drawPixel(x + (int)(cos(i) * (r-5)), 
                      y + (int)(sin(i) * (r-5)), 
                      ILI9340_BRIGHT_BLUE); 
    }
}


void OneWire_Init(OneWire_t* OneWireStruct, GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{
    HAL_TIM_Base_Start(&_DS18B20_H_TIMER);

    OneWireStruct->GPIOx = GPIOx;
    OneWireStruct->GPIO_Pin = GPIO_Pin;
    ONEWIRE_OUTPUT(OneWireStruct);
    ONEWIRE_HIGH(OneWireStruct);
    

    float start_angle = 0., end_angle = 2*M_PI;
    int x = (128/2)-1, y = (160/2)-1, r = 30;
    int N = (int)(2*M_PI)/0.05;
    
    funny_delay(x, y, r, start_angle, end_angle/4., N, 1000);
    
    ONEWIRE_LOW(OneWireStruct);

    funny_delay(x, y, r, end_angle/4., end_angle/2., N, 1000);

    ONEWIRE_HIGH(OneWireStruct);
    
    funny_delay(x, y, r, end_angle/2., end_angle, N, 2000);

}

uint8_t OneWire_Reset(OneWire_t* OneWireStruct)
{
    uint8_t i;

    /* Line low, and wait 480us */
    ONEWIRE_LOW(OneWireStruct);
    ONEWIRE_OUTPUT(OneWireStruct);
    ONEWIRE_DELAY_480();
    ONEWIRE_DELAY_20();
    /* Release line and wait for 70us */
    ONEWIRE_INPUT(OneWireStruct);
    ONEWIRE_DELAY_60();
    ONEWIRE_DELAY_10();
    /* Check bit value */
    i = HAL_GPIO_ReadPin(OneWireStruct->GPIOx, OneWireStruct->GPIO_Pin);

    /* Delay for 410 us */
    ONEWIRE_DELAY_100();
    ONEWIRE_DELAY_100();
    ONEWIRE_DELAY_100();
    ONEWIRE_DELAY_100();
    ONEWIRE_DELAY_10();
    /* Return value of presence pulse, 0 = OK, 1 = ERROR */
    return i;
}

void OneWire_WriteBit(OneWire_t* OneWireStruct, uint8_t bit)
{
    if (bit) {
        /* Set line low */
        ONEWIRE_LOW(OneWireStruct);
        ONEWIRE_OUTPUT(OneWireStruct);
        ONEWIRE_DELAY_10();

        /* Bit high */
        ONEWIRE_INPUT(OneWireStruct);

        /* Wait for 55 us and release the line */
        ONEWIRE_DELAY_60();
        ONEWIRE_INPUT(OneWireStruct);
    }
    else {
        /* Set line low */
        ONEWIRE_LOW(OneWireStruct);
        ONEWIRE_OUTPUT(OneWireStruct);
        ONEWIRE_DELAY_60();

        /* Bit high */
        ONEWIRE_INPUT(OneWireStruct);

        /* Wait for 5 us and release the line */
        ONEWIRE_DELAY_5();
        ONEWIRE_INPUT(OneWireStruct);
    }
}

uint8_t OneWire_ReadBit(OneWire_t* OneWireStruct)
{
    uint8_t bit = 0;

    /* Line low */
    ONEWIRE_LOW(OneWireStruct);
    ONEWIRE_OUTPUT(OneWireStruct);
    ONEWIRE_DELAY_2();

    /* Release line */
    ONEWIRE_INPUT(OneWireStruct);
    ONEWIRE_DELAY_10();

    /* Read line value */
    if (HAL_GPIO_ReadPin(OneWireStruct->GPIOx, OneWireStruct->GPIO_Pin)) {
        /* Bit is HIGH */
        bit = 1;
    }

    /* Wait 50us to complete 60us period */
    ONEWIRE_DELAY_60();

    /* Return bit value */
    return bit;
}

void OneWire_WriteByte(OneWire_t* OneWireStruct, uint8_t byte)
{
    uint8_t i = 8;
    /* Write 8 bits */
    while (i--) {
        /* LSB bit is first */
        OneWire_WriteBit(OneWireStruct, byte & 0x01);
        byte >>= 1;
    }
}

uint8_t OneWire_ReadByte(OneWire_t* OneWireStruct)
{
    uint8_t i = 8, byte = 0;
    while (i--) {
        byte >>= 1;
        byte |= (OneWire_ReadBit(OneWireStruct) << 7);
    }

    return byte;
}

uint8_t OneWire_First(OneWire_t* OneWireStruct)
{
    /* Reset search values */
    OneWire_ResetSearch(OneWireStruct);

    /* Start with searching */
    return OneWire_Search(OneWireStruct, ONEWIRE_CMD_SEARCHROM);
}

uint8_t OneWire_Next(OneWire_t* OneWireStruct)
{
    /* Leave the search state alone */
    return OneWire_Search(OneWireStruct, ONEWIRE_CMD_SEARCHROM);
}

void OneWire_ResetSearch(OneWire_t* OneWireStruct)
{
    /* Reset the search state */
    OneWireStruct->LastDiscrepancy = 0;
    OneWireStruct->LastDeviceFlag = 0;
    OneWireStruct->LastFamilyDiscrepancy = 0;
}

uint8_t OneWire_Search(OneWire_t* OneWireStruct, uint8_t command)
{
    uint8_t id_bit_number;
    uint8_t last_zero, rom_byte_number, search_result;
    uint8_t id_bit, cmp_id_bit;
    uint8_t rom_byte_mask, search_direction;

    /* Initialize for search */
    id_bit_number = 1;
    last_zero = 0;
    rom_byte_number = 0;
    rom_byte_mask = 1;
    search_result = 0;

    // if the last call was not the last one
    if (!OneWireStruct->LastDeviceFlag) {
        // 1-Wire reset
        if (OneWire_Reset(OneWireStruct)) {
            /* Reset the search */
            OneWireStruct->LastDiscrepancy = 0;
            OneWireStruct->LastDeviceFlag = 0;
            OneWireStruct->LastFamilyDiscrepancy = 0;
            return 0;
        }

        // issue the search command
        OneWire_WriteByte(OneWireStruct, command);

        // loop to do the search
        do {
            // read a bit and its complement
            id_bit = OneWire_ReadBit(OneWireStruct);
            cmp_id_bit = OneWire_ReadBit(OneWireStruct);

            // check for no devices on 1-wire
            if ((id_bit == 1) && (cmp_id_bit == 1)) {
                break;
            }
            else {
                // all devices coupled have 0 or 1
                if (id_bit != cmp_id_bit) {
                    search_direction = id_bit; // bit write value for search
                }
                else {
                    // if this discrepancy if before the Last Discrepancy
                    // on a previous next then pick the same as last time
                    if (id_bit_number < OneWireStruct->LastDiscrepancy) {
                        search_direction = ((OneWireStruct->ROM_NO[rom_byte_number] & rom_byte_mask) > 0);
                    }
                    else {
                        // if equal to last pick 1, if not then pick 0
                        search_direction = (id_bit_number == OneWireStruct->LastDiscrepancy);
                    }

                    // if 0 was picked then record its position in LastZero
                    if (search_direction == 0) {
                        last_zero = id_bit_number;

                        // check for Last discrepancy in family
                        if (last_zero < 9) {
                            OneWireStruct->LastFamilyDiscrepancy = last_zero;
                        }
                    }
                }

                // set or clear the bit in the ROM byte rom_byte_number
                // with mask rom_byte_mask
                if (search_direction == 1) {
                    OneWireStruct->ROM_NO[rom_byte_number] |= rom_byte_mask;
                }
                else {
                    OneWireStruct->ROM_NO[rom_byte_number] &= ~rom_byte_mask;
                }

                // serial number search direction write bit
                OneWire_WriteBit(OneWireStruct, search_direction);

                // increment the byte counter id_bit_number
                // and shift the mask rom_byte_mask
                id_bit_number++;
                rom_byte_mask <<= 1;

                // if the mask is 0 then go to new SerialNum byte rom_byte_number and reset mask
                if (rom_byte_mask == 0) {
                    //docrc8(ROM_NO[rom_byte_number]);  // accumulate the CRC
                    rom_byte_number++;
                    rom_byte_mask = 1;
                }
            }
        } while (rom_byte_number < 8); // loop until through all ROM bytes 0-7

        // if the search was successful then
        if (!(id_bit_number < 65)) {
            // search successful so set LastDiscrepancy,LastDeviceFlag,search_result
            OneWireStruct->LastDiscrepancy = last_zero;

            // check for last device
            if (OneWireStruct->LastDiscrepancy == 0) {
                OneWireStruct->LastDeviceFlag = 1;
            }

            search_result = 1;
        }
    }

    // if no device found then reset counters so next 'search' will be like a first
    if (!search_result || !OneWireStruct->ROM_NO[0]) {
        OneWireStruct->LastDiscrepancy = 0;
        OneWireStruct->LastDeviceFlag = 0;
        OneWireStruct->LastFamilyDiscrepancy = 0;
        search_result = 0;
    }

    return search_result;
}

int OneWire_Verify(OneWire_t* OneWireStruct)
{
    unsigned char rom_backup[8];
    int i, rslt, ld_backup, ldf_backup, lfd_backup;

    // keep a backup copy of the current state
    for (i = 0; i < 8; i++)
        rom_backup[i] = OneWireStruct->ROM_NO[i];
    ld_backup = OneWireStruct->LastDiscrepancy;
    ldf_backup = OneWireStruct->LastDeviceFlag;
    lfd_backup = OneWireStruct->LastFamilyDiscrepancy;

    // set search to find the same device
    OneWireStruct->LastDiscrepancy = 64;
    OneWireStruct->LastDeviceFlag = 0;

    if (OneWire_Search(OneWireStruct, ONEWIRE_CMD_SEARCHROM)) {
        // check if same device found
        rslt = 1;
        for (i = 0; i < 8; i++) {
            if (rom_backup[i] != OneWireStruct->ROM_NO[i]) {
                rslt = 1;
                break;
            }
        }
    }
    else {
        rslt = 0;
    }

    // restore the search state
    for (i = 0; i < 8; i++) {
        OneWireStruct->ROM_NO[i] = rom_backup[i];
    }
    OneWireStruct->LastDiscrepancy = ld_backup;
    OneWireStruct->LastDeviceFlag = ldf_backup;
    OneWireStruct->LastFamilyDiscrepancy = lfd_backup;

    // return the result of the verify
    return rslt;
}

void OneWire_TargetSetup(OneWire_t* OneWireStruct, uint8_t family_code)
{
    uint8_t i;

    // set the search state to find SearchFamily type devices
    OneWireStruct->ROM_NO[0] = family_code;
    for (i = 1; i < 8; i++) {
        OneWireStruct->ROM_NO[i] = 0;
    }

    OneWireStruct->LastDiscrepancy = 64;
    OneWireStruct->LastFamilyDiscrepancy = 0;
    OneWireStruct->LastDeviceFlag = 0;
}

void OneWire_FamilySkipSetup(OneWire_t* OneWireStruct)
{
    // set the Last discrepancy to last family discrepancy
    OneWireStruct->LastDiscrepancy = OneWireStruct->LastFamilyDiscrepancy;
    OneWireStruct->LastFamilyDiscrepancy = 0;

    // check for end of list
    if (OneWireStruct->LastDiscrepancy == 0) {
        OneWireStruct->LastDeviceFlag = 1;
    }
}

uint8_t OneWire_GetROM(OneWire_t* OneWireStruct, uint8_t index)
{
    return OneWireStruct->ROM_NO[index];
}

void OneWire_Select(OneWire_t* OneWireStruct, uint8_t* addr)
{
    uint8_t i;
    OneWire_WriteByte(OneWireStruct, ONEWIRE_CMD_MATCHROM);

    for (i = 0; i < 8; i++) {
        OneWire_WriteByte(OneWireStruct, *(addr + i));
    }
}

void OneWire_SelectWithPointer(OneWire_t* OneWireStruct, uint8_t* ROM)
{
    uint8_t i;
    OneWire_WriteByte(OneWireStruct, ONEWIRE_CMD_MATCHROM);

    for (i = 0; i < 8; i++) {
        OneWire_WriteByte(OneWireStruct, *(ROM + i));
    }
}

void OneWire_GetFullROM(OneWire_t* OneWireStruct, uint8_t* firstIndex)
{
    uint8_t i;
    for (i = 0; i < 8; i++) {
        *(firstIndex + i) = OneWireStruct->ROM_NO[i];
    }
}

uint8_t OneWire_CRC8(uint8_t* addr, uint8_t len)
{
    uint8_t crc = 0, inbyte, i, mix;

    while (len--) {
        inbyte = *addr++;
        for (i = 8; i; i--) {
            mix = (crc ^ inbyte) & 0x01;
            crc >>= 1;
            if (mix) {
                crc ^= 0x8C;
            }
            inbyte >>= 1;
        }
    }

    /* Return calculated CRC */
    return crc;
}

//###########################################################################################
uint8_t DS18B20_Start(OneWire_t* OneWire, uint8_t* ROM)
{
    /* Check if device is DS18B20 */
    if (!DS18B20_Is(ROM)) {
        return 0;
    }

    /* Reset line */
    OneWire_Reset(OneWire);
    /* Select ROM number */
    OneWire_SelectWithPointer(OneWire, ROM);
    /* Start temperature conversion */
    OneWire_WriteByte(OneWire, DS18B20_CMD_CONVERTTEMP);

    return 1;
}

void DS18B20_StartAll(OneWire_t* OneWire)
{
    /* Reset pulse */
    OneWire_Reset(OneWire);
    /* Skip rom */
    OneWire_WriteByte(OneWire, ONEWIRE_CMD_SKIPROM);
    /* Start conversion on all connected devices */
    OneWire_WriteByte(OneWire, DS18B20_CMD_CONVERTTEMP);
}

bool DS18B20_Read(OneWire_t* OneWire, uint8_t* ROM, float* destination)
{
    uint16_t temperature;
    uint8_t resolution;
    int8_t digit, minus = 0;
    float decimal;
    uint8_t i = 0;
    uint8_t data[9];
    uint8_t crc;

    /* Check if device is DS18B20 */
    if (!DS18B20_Is(ROM)) {
        return false;
    }

    /* Check if line is released, if it is, then conversion is complete */
    if (!OneWire_ReadBit(OneWire)) {
        /* Conversion is not finished yet */
        return false;
    }

    /* Reset line */
    OneWire_Reset(OneWire);
    /* Select ROM number */
    OneWire_SelectWithPointer(OneWire, ROM);
    /* Read scratchpad command by onewire protocol */
    OneWire_WriteByte(OneWire, ONEWIRE_CMD_RSCRATCHPAD);

    /* Get data */
    for (i = 0; i < 9; i++) {
        /* Read byte by byte */
        data[i] = OneWire_ReadByte(OneWire);
    }

    /* Calculate CRC */
    crc = OneWire_CRC8(data, 8);

    /* Check if CRC is ok */
    if (crc != data[8])
        /* CRC invalid */
        return 0;

    /* First two bytes of scratchpad are temperature values */
    temperature = data[0] | (data[1] << 8);

    /* Reset line */
    OneWire_Reset(OneWire);

    /* Check if temperature is negative */
    if (temperature & 0x8000) {
        /* Two's complement, temperature is negative */
        temperature = ~temperature + 1;
        minus = 1;
    }

    /* Get sensor resolution */
    resolution = ((data[4] & 0x60) >> 5) + 9;

    /* Store temperature integer digits and decimal digits */
    digit = temperature >> 4;
    digit |= ((temperature >> 8) & 0x7) << 4;

    /* Store decimal digits */
    switch (resolution) {
    case 9:
        decimal = (temperature >> 3) & 0x01;
        decimal *= (float)DS18B20_DECIMAL_STEPS_9BIT;
        break;
    case 10:
        decimal = (temperature >> 2) & 0x03;
        decimal *= (float)DS18B20_DECIMAL_STEPS_10BIT;
        break;
    case 11:
        decimal = (temperature >> 1) & 0x07;
        decimal *= (float)DS18B20_DECIMAL_STEPS_11BIT;
        break;
    case 12:
        decimal = temperature & 0x0F;
        decimal *= (float)DS18B20_DECIMAL_STEPS_12BIT;
        break;
    default:
        decimal = 0xFF;
        digit = 0;
    }

    /* Check for negative part */
    decimal = digit + decimal;
    if (minus)
        decimal = 0 - decimal;

    /* Set to pointer */
    *destination = decimal;

    /* Return 1, temperature valid */
    return true;
}

uint8_t DS18B20_GetResolution(OneWire_t* OneWire, uint8_t* ROM)
{
    uint8_t conf;

    if (!DS18B20_Is(ROM))
        return 0;

    /* Reset line */
    OneWire_Reset(OneWire);
    /* Select ROM number */
    OneWire_SelectWithPointer(OneWire, ROM);
    /* Read scratchpad command by onewire protocol */
    OneWire_WriteByte(OneWire, ONEWIRE_CMD_RSCRATCHPAD);

    /* Ignore first 4 bytes */
    OneWire_ReadByte(OneWire);
    OneWire_ReadByte(OneWire);
    OneWire_ReadByte(OneWire);
    OneWire_ReadByte(OneWire);

    /* 5th byte of scratchpad is configuration register */
    conf = OneWire_ReadByte(OneWire);

    /* Return 9 - 12 value according to number of bits */
    return ((conf & 0x60) >> 5) + 9;
}

uint8_t DS18B20_SetResolution(OneWire_t* OneWire, uint8_t* ROM, DS18B20_Resolution_t resolution)
{
    uint8_t th, tl, conf;
    if (!DS18B20_Is(ROM))
        return 0;

    /* Reset line */
    OneWire_Reset(OneWire);
    /* Select ROM number */
    OneWire_SelectWithPointer(OneWire, ROM);
    /* Read scratchpad command by onewire protocol */
    OneWire_WriteByte(OneWire, ONEWIRE_CMD_RSCRATCHPAD);

    /* Ignore first 2 bytes */
    OneWire_ReadByte(OneWire);
    OneWire_ReadByte(OneWire);

    th = OneWire_ReadByte(OneWire);
    tl = OneWire_ReadByte(OneWire);
    conf = OneWire_ReadByte(OneWire);

    if (resolution == DS18B20_Resolution_9bits) {
        conf &= ~(1 << DS18B20_RESOLUTION_R1);
        conf &= ~(1 << DS18B20_RESOLUTION_R0);
    }
    else if (resolution == DS18B20_Resolution_10bits) {
        conf &= ~(1 << DS18B20_RESOLUTION_R1);
        conf |= 1 << DS18B20_RESOLUTION_R0;
    }
    else if (resolution == DS18B20_Resolution_11bits) {
        conf |= 1 << DS18B20_RESOLUTION_R1;
        conf &= ~(1 << DS18B20_RESOLUTION_R0);
    }
    else if (resolution == DS18B20_Resolution_12bits) {
        conf |= 1 << DS18B20_RESOLUTION_R1;
        conf |= 1 << DS18B20_RESOLUTION_R0;
    }

    /* Reset line */
    OneWire_Reset(OneWire);
    /* Select ROM number */
    OneWire_SelectWithPointer(OneWire, ROM);
    /* Write scratchpad command by onewire protocol, only th, tl and conf register can be written */
    OneWire_WriteByte(OneWire, ONEWIRE_CMD_WSCRATCHPAD);

    /* Write bytes */
    OneWire_WriteByte(OneWire, th);
    OneWire_WriteByte(OneWire, tl);
    OneWire_WriteByte(OneWire, conf);

    /* Reset line */
    OneWire_Reset(OneWire);
    /* Select ROM number */
    OneWire_SelectWithPointer(OneWire, ROM);
    /* Copy scratchpad to EEPROM of DS18B20 */
    OneWire_WriteByte(OneWire, ONEWIRE_CMD_CPYSCRATCHPAD);

    return 1;
}

uint8_t DS18B20_Is(uint8_t* ROM)
{
    /* Checks if first byte is equal to DS18B20's family code */
    if (*ROM == DS18B20_FAMILY_CODE)
        return 1;

    return 0;
}

uint8_t DS18B20_SetAlarmLowTemperature(OneWire_t* OneWire, uint8_t* ROM, int8_t temp)
{
    uint8_t tl, th, conf;
    if (!DS18B20_Is(ROM))
        return 0;

    if (temp > 125)
        temp = 125;

    if (temp < -55)
        temp = -55;

    /* Reset line */
    OneWire_Reset(OneWire);
    /* Select ROM number */
    OneWire_SelectWithPointer(OneWire, ROM);
    /* Read scratchpad command by onewire protocol */
    OneWire_WriteByte(OneWire, ONEWIRE_CMD_RSCRATCHPAD);

    /* Ignore first 2 bytes */
    OneWire_ReadByte(OneWire);
    OneWire_ReadByte(OneWire);

    th = OneWire_ReadByte(OneWire);
    tl = OneWire_ReadByte(OneWire);
    conf = OneWire_ReadByte(OneWire);

    tl = (uint8_t)temp;

    /* Reset line */
    OneWire_Reset(OneWire);
    /* Select ROM number */
    OneWire_SelectWithPointer(OneWire, ROM);
    /* Write scratchpad command by onewire protocol, only th, tl and conf register can be written */
    OneWire_WriteByte(OneWire, ONEWIRE_CMD_WSCRATCHPAD);

    /* Write bytes */
    OneWire_WriteByte(OneWire, th);
    OneWire_WriteByte(OneWire, tl);
    OneWire_WriteByte(OneWire, conf);

    /* Reset line */
    OneWire_Reset(OneWire);
    /* Select ROM number */
    OneWire_SelectWithPointer(OneWire, ROM);
    /* Copy scratchpad to EEPROM of DS18B20 */
    OneWire_WriteByte(OneWire, ONEWIRE_CMD_CPYSCRATCHPAD);

    return 1;
}

uint8_t DS18B20_SetAlarmHighTemperature(OneWire_t* OneWire, uint8_t* ROM, int8_t temp)
{
    uint8_t tl, th, conf;
    if (!DS18B20_Is(ROM))
        return 0;

    if (temp > 125)
        temp = 125;

    if (temp < -55)
        temp = -55;

    /* Reset line */
    OneWire_Reset(OneWire);
    /* Select ROM number */
    OneWire_SelectWithPointer(OneWire, ROM);
    /* Read scratchpad command by onewire protocol */
    OneWire_WriteByte(OneWire, ONEWIRE_CMD_RSCRATCHPAD);

    /* Ignore first 2 bytes */
    OneWire_ReadByte(OneWire);
    OneWire_ReadByte(OneWire);

    th = OneWire_ReadByte(OneWire);
    tl = OneWire_ReadByte(OneWire);
    conf = OneWire_ReadByte(OneWire);

    th = (uint8_t)temp;

    /* Reset line */
    OneWire_Reset(OneWire);
    /* Select ROM number */
    OneWire_SelectWithPointer(OneWire, ROM);
    /* Write scratchpad command by onewire protocol, only th, tl and conf register can be written */
    OneWire_WriteByte(OneWire, ONEWIRE_CMD_WSCRATCHPAD);

    /* Write bytes */
    OneWire_WriteByte(OneWire, th);
    OneWire_WriteByte(OneWire, tl);
    OneWire_WriteByte(OneWire, conf);

    /* Reset line */
    OneWire_Reset(OneWire);
    /* Select ROM number */
    OneWire_SelectWithPointer(OneWire, ROM);
    /* Copy scratchpad to EEPROM of DS18B20 */
    OneWire_WriteByte(OneWire, ONEWIRE_CMD_CPYSCRATCHPAD);

    return 1;
}

uint8_t DS18B20_DisableAlarmTemperature(OneWire_t* OneWire, uint8_t* ROM)
{
    uint8_t tl, th, conf;
    if (!DS18B20_Is(ROM))
        return 0;

    /* Reset line */
    OneWire_Reset(OneWire);
    /* Select ROM number */
    OneWire_SelectWithPointer(OneWire, ROM);
    /* Read scratchpad command by onewire protocol */
    OneWire_WriteByte(OneWire, ONEWIRE_CMD_RSCRATCHPAD);

    /* Ignore first 2 bytes */
    OneWire_ReadByte(OneWire);
    OneWire_ReadByte(OneWire);

    th = OneWire_ReadByte(OneWire);
    tl = OneWire_ReadByte(OneWire);
    conf = OneWire_ReadByte(OneWire);

    th = 125;
    tl = (uint8_t)-55;

    /* Reset line */
    OneWire_Reset(OneWire);
    /* Select ROM number */
    OneWire_SelectWithPointer(OneWire, ROM);
    /* Write scratchpad command by onewire protocol, only th, tl and conf register can be written */
    OneWire_WriteByte(OneWire, ONEWIRE_CMD_WSCRATCHPAD);

    /* Write bytes */
    OneWire_WriteByte(OneWire, th);
    OneWire_WriteByte(OneWire, tl);
    OneWire_WriteByte(OneWire, conf);

    /* Reset line */
    OneWire_Reset(OneWire);
    /* Select ROM number */
    OneWire_SelectWithPointer(OneWire, ROM);
    /* Copy scratchpad to EEPROM of DS18B20 */
    OneWire_WriteByte(OneWire, ONEWIRE_CMD_CPYSCRATCHPAD);

    return 1;
}

uint8_t DS18B20_AlarmSearch(OneWire_t* OneWire)
{
    /* Start alarm search */
    return OneWire_Search(OneWire, DS18B20_CMD_ALARMSEARCH);
}

uint8_t DS18B20_AllDone(OneWire_t* OneWire)
{
    /* If read bit is low, then device is not finished yet with calculation temperature */
    return OneWire_ReadBit(OneWire);
}

float convertTemperToFloat(u8* p)
{
    uint8_t LSB, MSB;
    float data;
    uint16_t temperature;

    LSB = p[0];
    MSB = p[1];

    temperature = (MSB << 8) | LSB;

    if (temperature & 0x8000) {
        temperature = ~temperature + 1;
        data = 0.0 - (temperature / 16.0);
        return data;
    }
    data = temperature / 16.0;

    return data;
}

fix4 get_temperature(OneWire_t* OneWire)
{
    static u8 scratchpad[2] = { 0 };

    OneWire_Reset(OneWire);
    OneWire_WriteByte(OneWire, DS18B20_SKIP_ROM);
    OneWire_WriteByte(OneWire, DS18B20_CONVERT_T);

    WAIT_UNTIL_CONVERTION_COMPLETE(OneWire);

    OneWire_Reset(OneWire);
    OneWire_WriteByte(OneWire, DS18B20_SKIP_ROM);
    OneWire_WriteByte(OneWire, DS18B20_READ_SCRATCHPAD);

    scratchpad[0] = OneWire_ReadByte(OneWire);
    scratchpad[1] = OneWire_ReadByte(OneWire);

    return (fix4)((scratchpad[1] << 8) | scratchpad[0]);
    // return convertTemperToFloat((u8*)scratchpad);
}
