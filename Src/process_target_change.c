#include "contiki.h"
#include "common_types.h"
#include "main.h"
#include <sys/stimer.h>

PROCESS(target_change_process, "Target changing");

extern fix4 T_target, mean;

/*---------------------------------------------------------------------------*/
PROCESS_THREAD(target_change_process, ev, data)
{
    PROCESS_BEGIN();
    static struct stimer st;
    static fix4 original_target;

    original_target = T_target;

    PROCESS_PAUSE();

    T_target = mean;

    
    while (T_target > original_target) {

        T_target -= int2fix4(1);

        stimer_set(&st, S_IN_HOUR);
        PROCESS_WAIT_EVENT_UNTIL(stimer_expired(&st));
    }
    while (T_target < original_target) {

        T_target += int2fix4(1);

        stimer_set(&st, S_IN_HOUR);
        PROCESS_WAIT_EVENT_UNTIL(stimer_expired(&st));
    }

    T_target = original_target;

    PROCESS_END();
}