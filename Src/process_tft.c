#include "contiki.h"
#include "common_types.h"
#include "main.h"
#include "stm32f4xx_hal.h"
#include "printf.h"
#include "DS18B20.h"
#include "tft.h"
#include "tft_gfx.h"
#include "ring.h"

#define AXIS_START_H 28
#define AXIS_START_V 60
#define TEMPER_ARR_SIZE 16

extern char buffer[80];
extern fix4 f, f_last_switch, f_max, f_min, rate, mean, sigma;
extern relay_state_t relay_state;
extern uint32_t relay_time_stamp, time_last_switch;
extern fix4 T_target, real_mean;
extern u8 freq;
extern fix4 array[TEMPER_ARR_SIZE];
extern const u8 text_area_vertical_size;
extern menu_t menu[MENU_MAX];
extern const char circle_radius;
extern uint8_t tft_state;

short f_to_tft(fix4 T);
fix4 tft_to_f(short y);
void led_toggle(void* p);
float expectation(fix4* var, size_t size);
float variance(fix4* var, size_t size);
float std_deviation(fix4* var, size_t size);
u32 mem_get(u32 page_addr);
void mem_store(u32 page_addr, u8 byte);
void mem_erase(u8 sector);
void mem_write_two(u8 sector, u32 addr_target, fix4 target, u32 addr_freq, u8 frequency);
PROCESS(tft_process, "TFT display");

static char freq_flag = 1;
float sum;


void tft_write_text_pos(uint16_t x, uint16_t y, uint16_t color, uint8_t size, char* s)
{
    tft_setCursor(x, y);
    tft_setTextColor(color);
    tft_setTextSize(size);
    tft_writeString(s);
}

static void tft_draw_axes()
{
    const u8 axes_distance_between = 25;
    const u8 axes_distance_between_v = 10;
    u8 tmp = 0;

    tft_setTextColor(ILI9340_GRAY1);
    draw_vertical(AXIS_START_V, ILI9340_TFTHEIGHT - 1, AXIS_START_H + 0 * axes_distance_between - 1);
    draw_vertical(AXIS_START_V, ILI9340_TFTHEIGHT - 1, AXIS_START_H + 1 * axes_distance_between - 1);
    draw_vertical(AXIS_START_V, ILI9340_TFTHEIGHT - 1, AXIS_START_H + 2 * axes_distance_between - 1);
    draw_vertical(AXIS_START_V, ILI9340_TFTHEIGHT - 1, AXIS_START_H + 3 * axes_distance_between - 1);
    draw_vertical(AXIS_START_V, ILI9340_TFTHEIGHT - 1, AXIS_START_H + 4 * axes_distance_between - 1);

    draw_horizontal(AXIS_START_H, ILI9340_TFTWIDTH - 1, ILI9340_TFTHEIGHT - 1 - 0 * axes_distance_between_v);
    draw_horizontal(AXIS_START_H, ILI9340_TFTWIDTH - 1, ILI9340_TFTHEIGHT - 1 - 1 * axes_distance_between_v);
    draw_horizontal(AXIS_START_H, ILI9340_TFTWIDTH - 1, ILI9340_TFTHEIGHT - 1 - 2 * axes_distance_between_v);
    draw_horizontal(AXIS_START_H, ILI9340_TFTWIDTH - 1, ILI9340_TFTHEIGHT - 1 - 3 * axes_distance_between_v);
    draw_horizontal(AXIS_START_H, ILI9340_TFTWIDTH - 1, ILI9340_TFTHEIGHT - 1 - 4 * axes_distance_between_v);
    draw_horizontal(AXIS_START_H, ILI9340_TFTWIDTH - 1, ILI9340_TFTHEIGHT - 1 - 5 * axes_distance_between_v);
    draw_horizontal(AXIS_START_H, ILI9340_TFTWIDTH - 1, ILI9340_TFTHEIGHT - 1 - 6 * axes_distance_between_v);
    draw_horizontal(AXIS_START_H, ILI9340_TFTWIDTH - 1, ILI9340_TFTHEIGHT - 1 - 7 * axes_distance_between_v);
    draw_horizontal(AXIS_START_H, ILI9340_TFTWIDTH - 1, ILI9340_TFTHEIGHT - 1 - 8 * axes_distance_between_v);
    draw_horizontal(AXIS_START_H, ILI9340_TFTWIDTH - 1, ILI9340_TFTHEIGHT - 1 - 9 * axes_distance_between_v);
    draw_horizontal(AXIS_START_H, ILI9340_TFTWIDTH - 1, ILI9340_TFTHEIGHT - 1 - 10 * axes_distance_between_v);

    tmp = AXIS_START_V + 0 * axes_distance_between - 1;
    tft_setCursor(0, tmp);
    sprintf(buffer, "%0.1f", 25.); // 22.3 || 15
    tft_writeString(buffer);

    tmp = AXIS_START_V + 1 * axes_distance_between - 1;
    tft_setCursor(0, tmp);
    sprintf(buffer, "%0.1f", 17.5); // 16.2 || 10
    tft_writeString(buffer);

    tmp = AXIS_START_V + 2 * axes_distance_between - 1;
    tft_setCursor(0, tmp);
    sprintf(buffer, "%0.1f", 10.); //10.1 || 5
    tft_writeString(buffer);

    tmp = AXIS_START_V + 3 * axes_distance_between - 1;
    tft_setCursor(0, tmp);
    sprintf(buffer, "%0.1f", 2.5); // 4. || 0
    tft_writeString(buffer);

    tmp = AXIS_START_V + 4 * axes_distance_between - 1 - 8;
    tft_setCursor(0, tmp);
    sprintf(buffer, "%0.1f", -5.); // 4. || 0
    tft_writeString(buffer);
}


static void tft_draw_main_info()
{

    tft_setCursor(0, 0);
    tft_setTextColor(ILI9340_WHITE);
    tft_setTextSize(1);
    sprintf(buffer, "time since boot");
    tft_writeString(buffer);

    tft_setCursor(0, 10);
    tft_setTextColor(ILI9340_YELLOW);
    tft_setTextSize(1);
    sprintf(buffer, "%d", clock_seconds());
    tft_writeString(buffer);

    //tft_fillRoundRect(0,10, 110, 40, 1, ILI9340_BLACK);// x,y,w,h,radius,color
    tft_setCursor(0, 20);
    tft_setTextColor(ILI9340_CYAN);
    tft_setTextSize(1);
    sprintf(buffer, "T=%.1f -> Target=%.1f\r\n", fix2float4(f), fix2float4(T_target));
    tft_writeString(buffer);
        
    tft_setCursor(0, 40);
    tft_setTextColor(ILI9340_BLUE);
    tft_setTextSize(1);
    sprintf(buffer, "relay %s %dmin", (sWAIT == relay_state) ? "WAIT" : (sRELAY_ON == relay_state) ? "ON" : "OFF",
        (clock_seconds() - relay_time_stamp) / S_IN_MIN);
    tft_writeString(buffer);

    tft_setCursor(0, 50);
    tft_setTextColor(ILI9340_YELLOW);
    tft_setTextSize(1);
    sprintf(buffer, "mean=%.1f", fix2float4(real_mean));
    tft_writeString(buffer);
}



static void tft_draw_additional_info(fix4 f_max_, fix4 f_min_, u32 max_time)
{
    char t8 = 10;
    tft_setCursor(0, 30);
    tft_setTextSize(1);

    sprintf(buffer, menu[TARGET].string, fix2float4(T_target));
    tft_write_text_pos(0, t8, ILI9340_GRAY1, 1, buffer);
    t8 += 10;
    
    if (menu[T_MAX].state) {
        sprintf(buffer, menu[T_MAX].string, fix2float4(f_max_));
        tft_write_text_pos(0, t8, ILI9340_GRAY1, 1, buffer);
        t8 += 10;
    }
    if (menu[T_MIN].state) {
        sprintf(buffer, menu[T_MIN].string, fix2float4(f_min_));
        tft_write_text_pos(0, t8, ILI9340_GRAY1, 1, buffer);
        t8 += 10;
    }
    if (menu[MAX_TIME].state) {
        sprintf(buffer, menu[MAX_TIME].string, max_time / S_IN_MIN);
        tft_write_text_pos(0, t8, ILI9340_GRAY1, 1, buffer);
        t8 += 10;
    }
    if (menu[TMEAN].state) {
        sprintf(buffer, menu[TMEAN].string, fix2float4(mean));
        tft_write_text_pos(0, t8, ILI9340_GRAY1, 1, buffer);
        t8 += 10;
    }
    if (menu[SIGMA].state) {
        sprintf(buffer, menu[SIGMA].string, fix2float4(sigma));
        tft_write_text_pos(0, t8, ILI9340_GRAY1, 1, buffer);
        t8 += 10;
    }
    if (menu[RATE].state) {
        sprintf(buffer, menu[RATE].string, fix2float4(rate));
        tft_write_text_pos(0, t8, ILI9340_GRAY1, 1, buffer);
        t8 += 10;
    }
}

void tft_draw_curve(ring_t* buf)
{

    u16 head = ring_get_head(buf);
    u16 tmp = 0;
    fix4 fx;

    //tft_fillRoundRect(AXIS_START_H, AXIS_START_V, ILI9340_TFTWIDTH, ILI9340_TFTHEIGHT, 1, ILI9340_BLACK);
    tft_draw_axes();

    sum = 0.;
    for (int i = 0; i < ring_get_cnt(buf); i++) {
        tmp = ring_peek_char(buf, i);
        if (tmp) {
            if (&tft_means == buf) {
                tft_drawPixel(AXIS_START_H + i, tmp, ILI9340_YELLOW);
                fx = tft_to_f(tmp);
                sum += fix2float4(fx);
            }
            else if (&tft_rms == buf) {
                tft_drawPixel(AXIS_START_H + i, tmp, ILI9340_CYAN);
            }
            else {
                while (1)
                    ;
            }
        }
    }
    if (sum) {
        real_mean = float2fix4((float)sum / (float)ring_get_cnt(buf));
    }
}

void tft_clean_curve(ring_t* buf)
{

    u16 head = ring_get_head(buf);
    u16 tmp = 0;

    for (int i = 0; i < RING_SIZE; i++) {
        tmp = ring_peek_char(buf, i);
        if (tmp) {
            tft_drawPixel(AXIS_START_H + i, tmp, ILI9340_BLACK);
        }
    }
}

void tft_slow_data_old()
{
    static uint32_t max_time = 0;
    static uint16_t t16, x16;
    static uint8_t cnt_fullness = 0;
    static bool array_full = false;


    // ������� ������������� ����� �������� �����
    tft_fillRoundRect(0, 10, ILI9340_TFTWIDTH,
        text_area_vertical_size, 0, ILI9340_BLACK); // x,y,w,h,radius,color
    // ����� ����� -----------------------------------------
    
    if (BUTTON_NOT_PUSHED) {
        tft_draw_main_info();
    }
    
    if (f_max < f) {
        f_max = f;
    }
    if (f_min > f) {
        f_min = f;
    }
    if (max_time < (clock_seconds() - relay_time_stamp)) {
        max_time = (clock_seconds() - relay_time_stamp);
    }

    if (BUTTON_PUSHED) {
        tft_draw_additional_info(f_max, f_min, max_time);
    }
    // ����� ����� ����� -----------------------------------------

    if (0 == (clock_seconds()) % freq) {
        cnt_fullness++;

        array[cnt_fullness % TEMPER_ARR_SIZE] = f;
        // ��� ��������, ���������
        if (array_full == false) {
            mean = float2fix4(expectation(array, cnt_fullness));
            sigma = float2fix4(std_deviation(array, cnt_fullness));
            if (TEMPER_ARR_SIZE == cnt_fullness) {
                array_full = true;
            }
        }
        else {
            mean = float2fix4(expectation(array, TEMPER_ARR_SIZE));
            sigma = float2fix4(std_deviation(array, TEMPER_ARR_SIZE));

            if (RING_SIZE == tft_means.cnt) {
                if (freq_flag) {
                    freq_flag = 0;
                    freq = mem_get((u32)FLASH_USER_FREQ);
                }
            }
        }

        // ����� ������
        t16 = f_to_tft(f);
        x16 = f_to_tft(mean);
        
        ring_put_char(&tft_rms, t16);
        ring_put_char(&tft_means, x16);
    }
    if (BUTTON_PUSHED) {
        return;
    }
    tft_clean_curve(&tft_rms);
    tft_clean_curve(&tft_means);
    tft_draw_curve(&tft_rms);
    tft_draw_curve(&tft_means);
}




PROCESS_THREAD(tft_process, ev, data)
{
    PROCESS_BEGIN();


    static uint8_t tft_menu_line = 0;

    while (1) {

        PROCESS_WAIT_EVENT();
        switch (tft_state) {

        //
        case 0:

            tft_clean();
            if (ev == START_PUSHED_MSG) {
                tft_state = 1;
                tft_menu_line = 0;
                break;
            }
            if (ev == INFO_PUSHED_MSG) {
                tft_state = 2;
                tft_menu_line = 0;
                break;
            }

            tft_slow_data_old();
            break;
        case 1:
            tft_clean();

            // ������ ������ �������� ��������� ������ ����
            if (ev == START_PUSHED_MSG) {

              do {
                  if (NULL != strstr(menu[tft_menu_line].name, "..")) {
                      tft_state = 0;
                      
                      fix4 tfix4;
                      u8 t8;
                      
                      tfix4 = int2fix4(mem_get((u32)FLASH_USER_T_TARGET));
                      if (tfix4 != T_target) {
                          t8 = mem_get((u32)FLASH_USER_FREQ);
                          mem_erase(7);
                          mem_store(FLASH_USER_T_TARGET, fix2int4(T_target));
                          mem_store(FLASH_USER_FREQ, t8);
                      }
                      
                      t8 = mem_get((u32)FLASH_USER_FREQ);
                      if (t8 != freq) {
                          mem_erase(7);
                          mem_store(FLASH_USER_T_TARGET, fix2int4(T_target));
                          mem_store(FLASH_USER_FREQ, freq);
                      }
                      
                      break;
                  } else if (NULL != strstr(menu[tft_menu_line].name, "TARGET")) {
                      T_target += int2fix4(1);
                      if (T_target > int2fix4(18)) {
                          T_target = int2fix4(0);
                      }
                      break;
                  } else if (NULL != strstr(menu[tft_menu_line].name, "freq")) {

                      freq += 10;
                      break;
                  }
                  // ���������� ON OFF
                  else if (menu[tft_menu_line].string)
                  {
                      menu[tft_menu_line].state = !menu[tft_menu_line].state;
                      break;
                  }
                }
                while(0);
            }
            if (ev == SELECT_PUSHED_MSG) {
                tft_menu_line++;
                if (tft_menu_line > MENU_MAX-1) {
                    tft_menu_line = 0;
                }
            }

            for (int i = 0; i < MENU_MAX; i++) {
                sprintf(buffer, "%s", menu[i].name);

                if (NULL == strstr(menu[i].name, "..")) {
                    if (NULL != strstr(menu[i].name, "TARGET")){
                        sprintf(buffer, "%s: %0.1f", menu[i].name, fix2float4(T_target));
                    } 
                    else if (NULL != strstr(menu[i].name, "freq")){
                        sprintf(buffer, "%s: %d", menu[i].name, freq);
                    } 
                    else if (menu[i].state) {
                        strcat(buffer, " on");
                    } else {
                        strcat(buffer, " off");
                    }
                }

                if (i == tft_menu_line) {

                    tft_drawLine(2, 13 + 10 * i, 
                                 2 + circle_radius, 13 + 10 * i, ILI9340_GREEN);
                    
//                    tft_drawCircle(2, 13 + 10 * i, circle_radius, ILI9340_GREEN);
                    tft_write_text_pos(7, 10 + 10 * i, ILI9340_GREEN, 1, buffer);
                }
                else {
                    tft_write_text_pos(7, 10 + 10 * i, ILI9340_GRAY1, 1, buffer);
                }
            }

            break;
        } // endswitch
        
        if (TFT_WAKE_MSG == ev) {
            led_toggle(NULL);
        }
    } // endwhile(1)

    PROCESS_END();
}
