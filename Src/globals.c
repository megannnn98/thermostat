
#include "stm32f4xx_hal.h"
#include "common_types.h"
#include "DS18B20.h"
#include "tft.h"

#define TEMPER_ARR_SIZE 32


uint32_t relay_time_stamp = 0, time_last_switch;
u8 freq = 255;
OneWire_t OneWire;
fix4 f, f_last_switch, f_max, f_min, rate, mean, sigma;

fix4 array[TEMPER_ARR_SIZE];
char buffer[80];


relay_state_t relay_state;


uint8_t tft_state = 0;
fix4 T_target = float2fix4(10.);

const u8 text_area_vertical_size = 45;

fix4 T_target, real_mean;

enum {
NoPush = 0,
MaybePush,
Pushed,
MaybeNoPush
}select_push_state, start_push_state, info_push_state;

//typedef struct {
//    const char* name;
//    const char* string;
//    char  state;
//} menu_t;
menu_t menu[MENU_MAX] = {
{"..",       NULL, 1},
{"t_max",    "t_max=%0.1f", 1},
{"t_min",    "t_min=%0.1f", 1},
{"max_time", "max_time=%d", 1},
{"tmean",    "tmean=%0.1f", 1},
{"sigma",    "sigma=%0.1f", 1},
{"TARGET",   "TARGET=%0.1f", 1},
{"rate",     "rate=%.2fC/min", 1},
{"freq",     "freq=%0.1f", 1},
};
const char circle_radius = 2;

