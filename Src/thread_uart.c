
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f0xx_hal.h"

#include <string.h>
#include "pt.h"
#include "common_types.h"

void mem_store(u32 page_addr, u8 byte);

FlagStatus uart_rx;
uint32_t time_rx;
char cnt_rx;
fix4 T_target = float2fix4(12.),
     T_threshold_max = float2fix4(30.),
     T_threshold_min = float2fix4(0.);

extern fix4 f;
char buffer_rx[5];

extern UART_HandleTypeDef huart1;
extern u8 freq;
relay_state_t relay_state = sWAIT;

PT_THREAD(protothread_uart(pt_t* pt))
{
    PT_BEGIN(pt);

    while (1) {
        PT_YIELD(pt);
        if (SET == uart_rx) {

            while ((HAL_GetTick() - time_rx) < 10) {
                PT_YIELD(pt);
            }

            if ((cnt_rx > 2) && (0x05 == buffer_rx[0])) {

                T_target = int2fix4(buffer_rx[1] & 0x7F);
                T_threshold_min = int2fix4(buffer_rx[2] & 0x7F);
                T_threshold_max = int2fix4(buffer_rx[3] & 0x7F);
                freq = buffer_rx[4] & 0x7F;

                PRINT("0x%02x_0x%02x_0x%02x_0x%02x_0x%02x_OK",
                    buffer_rx[0], buffer_rx[1], buffer_rx[2], buffer_rx[3], buffer_rx[4]);

                mem_store(FLASH_USER_T_TARGET, fix2int4(T_target));
            }

            uart_rx = RESET;
            cnt_rx = 0;
            memset(buffer_rx, 0, sizeof(buffer_rx));
        }
    } // END WHILE(1)
    PT_END(pt);
} // timer thread
