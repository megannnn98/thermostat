
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"
#include "common_types.h"
#include "tft.h"
#include <math.h>
#include <sys/ctimer.h>

PROCESS_NAME(tft_process);

//State machine state names
extern enum {
  NoPush = 0,
  MaybePush,
  Pushed,
  MaybeNoPush
}select_push_state, start_push_state, info_push_state;

unsigned char select_push_flag; //message indicating a button push
unsigned char start_push_flag;  //message indicating a button push
unsigned char info_push_flag;  //message indicating a button push


float expectation(fix4* var, size_t size)
{
    float sum = 0;
    size_t i;

    for (i = 0; i < size; ++i) {
        sum += fix2float4(var[i]);
    }

    return sum / size;
}

float variance(fix4* var, size_t size)
{
    float sum = 0;
    float fmean = expectation(var, size);

    size_t i;

    for (i = 0; i < size; ++i) {
        sum += (fix2float4(var[i]) - fmean) * (fix2float4(var[i]) - fmean);
    }

    return sum / size;
}

float std_deviation(fix4* var, size_t size)
{
    return sqrt(variance(var, size));
}
void led_toggle(void* p) {
    LD2_GPIO_Port->ODR ^= LD2_Pin;
}


void mem_erase(u8 sector)
{
    HAL_FLASH_Unlock();

    __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR | FLASH_FLAG_PGSERR);

    FLASH_WaitForLastOperation((uint32_t)1000);
    FLASH_Erase_Sector( sector, (uint8_t) FLASH_TYPEERASE_SECTORS);
    FLASH_WaitForLastOperation((uint32_t)1000);
    CLEAR_BIT(FLASH->CR, (FLASH_CR_SER | FLASH_CR_SNB));

    HAL_FLASH_Lock();    
}

void mem_store(u32 page_addr, u8 byte)
{
    HAL_FLASH_Unlock();
    HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, page_addr, byte);
    HAL_FLASH_Lock();
}
u32 mem_get(u32 page_addr) {
    return (*(u32*)page_addr);
}

void mem_write_two(u8 sector, u32 addr_target, fix4 target, u32 addr_freq, u8 frequency) {
    mem_erase(sector);
    mem_store(addr_target, fix2int4(target));
    mem_store(addr_freq, frequency);
}


//*******************************
//Task 3
char set_state(char btn, unsigned char* PushState)
{
    char value = 0;
    if (btn == SELECT_BTN) {
        value = SELECT_PUSHED;
    }
    else {
        value = START_PUSHED;
    }

    switch (*PushState) {
    case NoPush:
        if (value)
            *PushState = MaybePush;
        else
            *PushState = NoPush;
        break;
    case MaybePush:
        if (value) {
            *PushState = Pushed;
            return 1;
        }
        else
            PushState = NoPush;
        break;
    case Pushed:
        if (value)
            *PushState = Pushed;
        else
            *PushState = MaybeNoPush;
        break;
    case MaybeNoPush:
        if (value)
            *PushState = Pushed;
        else {
            *PushState = NoPush;
            return 0;
        }
        break;
    }
    return -1;
}



void buttons_check(void* p) {
    select_push_flag = set_state(SELECT_BTN, (unsigned char*)&select_push_state);
    start_push_flag = set_state(START_BTN, (unsigned char*)&start_push_state);
    info_push_flag = set_state(INFO_BTN, (unsigned char*)&info_push_state);
    
    if (1 == select_push_flag) {
        process_post(&tft_process, SELECT_PUSHED_MSG, &select_push_state);
    } 
    if (1 == start_push_flag) {
        process_post(&tft_process, START_PUSHED_MSG, &start_push_state);
    } 

    ctimer_set((struct ctimer*)p, 30, buttons_check, (struct ctimer*)p);
}


short f_to_tft(fix4 T)
{
    //    return (short)((T*4.)+10);
    short t = 0;
    fix4 x = multfix4(T, int2fix4(3));
    x += int2fix4(15);
    t = ILI9340_TFTHEIGHT - (short)fix2int4(x);
    return t;
}
fix4 tft_to_f(short y)
{

    fix4 tmp = int2fix4(y);
    //    tmp *= -(193./750.);
    //    tmp += 14431./375;
    tmp = divfix4(tmp, int2fix4(-3));
    tmp += divfix4(int2fix4(145), int2fix4(3));

    return tmp;
}











