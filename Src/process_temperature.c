#include "contiki.h"
#include "common_types.h"
#include "main.h"
#include "stm32f4xx_hal.h"
#include "printf.h"
#include "DS18B20.h"

/*---------------------------------------------------------------------------*/
PROCESS(temperature_process, "Temperature");

extern relay_state_t relay_state;
extern OneWire_t OneWire;
extern fix4 f, f_last_switch, f_max, f_min, rate, mean, sigma, T_target;
extern fix4 array[];
extern uint8_t tft_state;
PROCESS_NAME(relay_process);
PROCESS_NAME(tft_process);
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(temperature_process, ev, data)
{
    PROCESS_BEGIN();
    static struct etimer et;
    static fix4 tfix;

    PROCESS_PAUSE();
    
    array[0] = mean = f = get_temperature(&OneWire);
    tfix = f_max = f_min = f;
    process_post(PROCESS_BROADCAST, 0x01, NULL);
        
    while (1) {

        OneWire_Reset(&OneWire);
        OneWire_WriteByte(&OneWire, DS18B20_SKIP_ROM);
        OneWire_WriteByte(&OneWire, DS18B20_CONVERT_T);

        etimer_set(&et, CLOCK_SECOND);
        PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et) || (!(0 == OneWire_ReadBit(&OneWire))));

        if (!etimer_expired(&et)) {
            continue;
        }
        
        OneWire_Reset(&OneWire);
        OneWire_WriteByte(&OneWire, DS18B20_SKIP_ROM);
        OneWire_WriteByte(&OneWire, DS18B20_READ_SCRATCHPAD);

        tfix = (fix4)OneWire_ReadByte(&OneWire);
        tfix |= (fix4)(OneWire_ReadByte(&OneWire) << 8);

        f = tfix;        
        
        
        if ((f > int2fix4(-5)) && (f < int2fix4(30))) {
            if (0 == tft_state) {
                process_post(&relay_process, RELAY_WAKE_MSG, NULL);
                PRINTF("\r\nT = %0.1f", fix2float4(f));
            }
        }
        process_post(&tft_process, TFT_WAKE_MSG, NULL);
    }

    PROCESS_END();
}

