
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f0xx_hal.h"
#include <string.h>
#include <math.h>
#include "tft.h"
#include "tft_gfx.h"
#include "DS18B20.h"
#include "pt.h"
#include "ring.h"

#define AXIS_START_H 28
#define AXIS_START_V 60
#define TEMPER_ARR_SIZE 32

uint32_t relay_time_stamp = 0, time_last_switch;
u8 freq = 100;
OneWire_t OneWire;
char buffer[80];

static fix4 array[TEMPER_ARR_SIZE];
static int sys_time_seconds;
static int flag_alarm = 0;
static bool t_ready_sem = false;
static const u8 text_area_vertical_size = 45;

extern relay_state_t relay_state;
extern fix4 T_target, T_threshold_max, T_threshold_min;
fix4 f, f_last_switch, f_max, f_min, rate, mean, sigma;

float expectation(fix4* var, size_t size);
float variance(fix4* var, size_t size);
float std_deviation(fix4* var, size_t size);

short f_to_tft(fix4 T)
{
    //    return (short)((T*4.)+10);
    short t = 0;
    fix4 x = multfix4(T, int2fix4(3));
    x += int2fix4(15);
    t = ILI9340_TFTHEIGHT - (short)fix2int4(x);
    return t;
}
fix4 tft_to_f(short y)
{

    fix4 tmp = int2fix4(y);
    //    tmp *= -(193./750.);
    //    tmp += 14431./375;
    tmp = divfix4(tmp, int2fix4(-3));
    tmp += divfix4(int2fix4(145), int2fix4(3));

    return tmp;
}

static void tft_draw_axes()
{

    const u8 axes_distance_between = 25;
    const u8 axes_distance_between_v = 10;
    u8 tmp = 0;

    tft_setTextColor(ILI9340_GRAY1);
    draw_vertical(AXIS_START_V, ILI9340_TFTHEIGHT - 1, AXIS_START_H + 0 * axes_distance_between - 1);
    draw_vertical(AXIS_START_V, ILI9340_TFTHEIGHT - 1, AXIS_START_H + 1 * axes_distance_between - 1);
    draw_vertical(AXIS_START_V, ILI9340_TFTHEIGHT - 1, AXIS_START_H + 2 * axes_distance_between - 1);
    draw_vertical(AXIS_START_V, ILI9340_TFTHEIGHT - 1, AXIS_START_H + 3 * axes_distance_between - 1);
    draw_vertical(AXIS_START_V, ILI9340_TFTHEIGHT - 1, AXIS_START_H + 4 * axes_distance_between - 1);

    draw_horizontal(AXIS_START_H, ILI9340_TFTWIDTH - 1, ILI9340_TFTHEIGHT - 1 - 0 * axes_distance_between_v);
    draw_horizontal(AXIS_START_H, ILI9340_TFTWIDTH - 1, ILI9340_TFTHEIGHT - 1 - 1 * axes_distance_between_v);
    draw_horizontal(AXIS_START_H, ILI9340_TFTWIDTH - 1, ILI9340_TFTHEIGHT - 1 - 2 * axes_distance_between_v);
    draw_horizontal(AXIS_START_H, ILI9340_TFTWIDTH - 1, ILI9340_TFTHEIGHT - 1 - 3 * axes_distance_between_v);
    draw_horizontal(AXIS_START_H, ILI9340_TFTWIDTH - 1, ILI9340_TFTHEIGHT - 1 - 4 * axes_distance_between_v);
    draw_horizontal(AXIS_START_H, ILI9340_TFTWIDTH - 1, ILI9340_TFTHEIGHT - 1 - 5 * axes_distance_between_v);
    draw_horizontal(AXIS_START_H, ILI9340_TFTWIDTH - 1, ILI9340_TFTHEIGHT - 1 - 6 * axes_distance_between_v);
    draw_horizontal(AXIS_START_H, ILI9340_TFTWIDTH - 1, ILI9340_TFTHEIGHT - 1 - 7 * axes_distance_between_v);
    draw_horizontal(AXIS_START_H, ILI9340_TFTWIDTH - 1, ILI9340_TFTHEIGHT - 1 - 8 * axes_distance_between_v);
    draw_horizontal(AXIS_START_H, ILI9340_TFTWIDTH - 1, ILI9340_TFTHEIGHT - 1 - 9 * axes_distance_between_v);
    draw_horizontal(AXIS_START_H, ILI9340_TFTWIDTH - 1, ILI9340_TFTHEIGHT - 1 - 10 * axes_distance_between_v);

    tmp = AXIS_START_V + 0 * axes_distance_between - 1;
    tft_setCursor(0, tmp);
    sprintf(buffer, "%0.1f", 25.); // 22.3 || 15
    tft_writeString(buffer);

    tmp = AXIS_START_V + 1 * axes_distance_between - 1;
    tft_setCursor(0, tmp);
    sprintf(buffer, "%0.1f", 17.5); // 16.2 || 10
    tft_writeString(buffer);

    tmp = AXIS_START_V + 2 * axes_distance_between - 1;
    tft_setCursor(0, tmp);
    sprintf(buffer, "%0.1f", 10.); //10.1 || 5
    tft_writeString(buffer);

    tmp = AXIS_START_V + 3 * axes_distance_between - 1;
    tft_setCursor(0, tmp);
    sprintf(buffer, "%0.1f", 2.5); // 4. || 0
    tft_writeString(buffer);

    tmp = AXIS_START_V + 4 * axes_distance_between - 1 - 8;
    tft_setCursor(0, tmp);
    sprintf(buffer, "%0.1f", -5.); // 4. || 0
    tft_writeString(buffer);
}

static void tft_draw_main_info()
{
    tft_setCursor(0, 10);
    tft_setTextColor(ILI9340_YELLOW);
    tft_setTextSize(1);
    sprintf(buffer, "%d", sys_time_seconds);
    tft_writeString(buffer);

    //tft_fillRoundRect(0,10, 110, 40, 1, ILI9340_BLACK);// x,y,w,h,radius,color
    tft_setCursor(0, 20);
    tft_setTextColor(ILI9340_CYAN);
    tft_setTextSize(1);
    sprintf(buffer, "T=%.1f", fix2float4(f));
    tft_writeString(buffer);

    tft_setTextColor(ILI9340_GRAY1);
    tft_setTextSize(1);
    sprintf(buffer, " relay %s %dmin", (sWAIT == relay_state) ? "WAIT" : (sRELAY_ON == relay_state) ? "ON" : "OFF",
        (HAL_GetTick() - relay_time_stamp) / MS_IN_MIN);
    tft_writeString(buffer);
}

static void tft_draw_additional_info(fix4 f_max_, fix4 f_min_, u32 max_time)
{

    static uint8_t other_info_state = 0;

    tft_setCursor(0, 30);
    tft_setTextSize(1);

    if ((0 == other_info_state) || (1 == other_info_state)) {
        other_info_state++;
        tft_setTextColor(ILI9340_GRAY1);
        sprintf(buffer, "t_max=%0.1f t_min=%0.1f\nmax_time=%d\n",
            fix2float4(f_max_), fix2float4(f_min_), max_time / MS_IN_MIN);
        tft_writeString(buffer);
        tft_setTextColor(ILI9340_YELLOW);
        sprintf(buffer, "mean=%0.1f ", fix2float4(mean));
        tft_writeString(buffer);
        tft_setTextColor(ILI9340_GRAY1);
        sprintf(buffer, "sigma=%0.1f", fix2float4(sigma));
        tft_writeString(buffer);
    }
    else if ((2 == other_info_state) || (3 == other_info_state)) {
        other_info_state++;

        if (4 == other_info_state) {
            other_info_state = 0;
        }
        if (flag_alarm) {
            tft_setTextColor(ILI9340_RED);
            sprintf(buffer, "TARGET=%.1f ALARM\nTHR_MAX=%.0f THR_MIN=%.0f\nrate=%.2fC/min",
                fix2float4(T_target), fix2float4(T_threshold_max), fix2float4(T_threshold_min), fix2float4(rate));
        }
        else {
            tft_setTextColor(ILI9340_GRAY1);
            sprintf(buffer, "TARGET=%.1f NO ALARM\nTHR_MAX=%.0f THR_MIN=%.0f\nrate=%.2fC/hour",
                fix2float4(T_target), fix2float4(T_threshold_max), fix2float4(T_threshold_min), fix2float4(rate));
        }
        tft_writeString(buffer);
    }
}

void tft_draw_curve(ring_t* buf)
{

    u16 head = ring_get_head(buf);
    u16 tmp = 0;

    //tft_fillRoundRect(AXIS_START_H, AXIS_START_V, ILI9340_TFTWIDTH, ILI9340_TFTHEIGHT, 1, ILI9340_BLACK);
    tft_draw_axes();

    for (int i = 0; i < ring_get_cnt(buf); i++) {
        tmp = ring_peek_char(buf, i);
        if (tmp) {
            if (&tft_means == buf) {
                tft_drawPixel(AXIS_START_H + i, tmp, ILI9340_YELLOW);
            }
            else if (&tft_rms == buf) {
                tft_drawPixel(AXIS_START_H + i, tmp, ILI9340_CYAN);
            }
            else {
                while (1)
                    ;
            }
        }
    }
}
void tft_clean_curve(ring_t* buf)
{

    u16 head = ring_get_head(buf);
    u16 tmp = 0;

    for (int i = 0; i < RING_SIZE; i++) {
        tmp = ring_peek_char(buf, i);
        if (tmp) {
            tft_drawPixel(AXIS_START_H + i, tmp, ILI9340_BLACK);
        }
    }
}

PT_THREAD(protothread_target_change(pt_t* pt))
{
    PT_BEGIN(pt);
    static fix4 original_target;
    static uint32_t time_stamp;

    original_target = T_target;

    PT_WAIT_UNTIL(pt, t_ready_sem == true);

    T_target = mean;
    while (T_target > original_target) {

        T_target -= int2fix4(1);
        PT_DELAY_MS(time_stamp, MS_IN_HOUR);
    }

    T_target = original_target;

    while (1) {
        PT_YIELD(pt);
    }

    PT_END(pt);
} // timer thread

// === Timer Thread =================================================
// update a 1 second tick counter
PT_THREAD(protothread_timer(pt_t* pt))
{
    PT_BEGIN(pt);

    static uint32_t time_stamp = 0, max_time = 0;
    static uint16_t tmp;
    //    volatile static short last_p_x = 0, last_p_y = 0;
    static uint8_t cnt_fullness = 0;
    static bool array_full = false;

    tft_setRotation(4);
    tft_setCursor(0, 0);
    tft_setTextColor(ILI9340_WHITE);
    tft_setTextSize(1);
    tft_writeString((char*)"Time since boot\n");

    //    last_p_x = (2+sys_time_seconds)%ILI9340_TFTWIDTH;
    //    last_p_y = ILI9340_TFTHEIGHT-f_to_tft(f);

    f_max = f_min = f;

    PRINT("%s", "thread timer go");

    tft_draw_axes();

    PT_WAIT_UNTIL(pt, t_ready_sem == true);

    while (1) {
        // yield time 1 second
        PT_DELAY_MS(time_stamp, MS_IN_SEC);

        HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_12);
        sys_time_seconds++;

        //        void tft_fillRoundRect(short x, short y, short w,
        //				 short h, short r, unsigned short color);
        // ������� ������������� ����� �������� �����
        tft_fillRoundRect(0, 10, ILI9340_TFTWIDTH,
            text_area_vertical_size, 1, ILI9340_BLACK); // x,y,w,h,radius,color
        // ����� ����� -----------------------------------------
        tft_draw_main_info();

        if (f_max < f) {
            f_max = f;
        }
        if (f_min > f) {
            f_min = f;
        }
        if (max_time < (HAL_GetTick() - relay_time_stamp)) {
            max_time = (HAL_GetTick() - relay_time_stamp);
        }
        if (f < T_threshold_max) {
            flag_alarm = 0;
        }
        else {
            flag_alarm = 1;
        }

        tft_draw_additional_info(f_max, f_min, max_time);
        // ����� ����� ����� -----------------------------------------

        if (0 == (sys_time_seconds) % freq) {
            cnt_fullness++;

            array[cnt_fullness % TEMPER_ARR_SIZE] = f;
            // ��� ��������, ���������
            if (array_full == false) {
                mean = float2fix4(expectation(array, cnt_fullness));
                sigma = float2fix4(std_deviation(array, cnt_fullness));
                if (TEMPER_ARR_SIZE == cnt_fullness) {
                    array_full = true;
                }
            }
            else {
                mean = float2fix4(expectation(array, TEMPER_ARR_SIZE));
                sigma = float2fix4(std_deviation(array, TEMPER_ARR_SIZE));
            }

            tft_clean_curve(&tft_rms);
            tft_clean_curve(&tft_means);
            //            // ����� ������
            tmp = f_to_tft(f);
            ring_put_char(&tft_rms, tmp);
            tft_draw_curve(&tft_rms);

            tmp = f_to_tft(mean);
            ring_put_char(&tft_means, tmp);
            tft_draw_curve(&tft_means);
        }
        // NEVER exit while
    } // END WHILE(1)
    PT_END(pt);
} // timer thread

PT_THREAD(protothread_relay(pt_t* pt))
{
    static fix4 diff;
    static u32 t32;

    if ((flag_alarm) && (sWAIT != relay_state)) {
        RELAY_ON;
        return PT_ENDED;
    }
    PT_BEGIN(pt);

    PT_WAIT_UNTIL(pt, t_ready_sem == true);

    PRINT("thread relay go");
    while (1) {

        // ����� �� ����� ����� ��������
        relay_time_stamp = HAL_GetTick();
        PT_WAIT_UNTIL(pt, (HAL_GetTick() - relay_time_stamp) > (10 * MS_IN_MIN - 1));
        //  PT_WAIT_UNTIL(pt, (HAL_GetTick() - relay_time_stamp) > 3*MS_IN_SEC);

        //            sprintf(buffer,"%s", "relay on");
        //            HAL_UART_Transmit(&huart1, (uint8_t*)buffer, strlen(buffer), 1000);

        // ����� ��������

        // ����������� ������ - ���� ��������
        if (mean > T_target) {
            // � ���� ��������� - �� ��������� - ������� � ���� ��� 10 �����
            if (sRELAY_OFF == relay_state) {
                RELAY_ON;
                //tft_drawCircle(ring_get_cnt(&tft_rms), f_to_tft(f), 3, ILI9340_RED);
            }
            // � ���� �������� - ���� ����� ������� �� ������ �����������
            else {
                if (sWAIT != relay_state) {
                    PT_WAIT_WHILE(pt, mean > T_target);
                }
                RELAY_OFF;
                //tft_drawCircle(ring_get_cnt(&tft_rms), f_to_tft(f), 3, ILI9340_GREEN);
            }
        }
        // ����������� ����� - ���� ���������
        else {
            // � ���� ��������� - ����������� - ����� ��������� �� ������ �����������
            if (sRELAY_OFF == relay_state) {
                if (sWAIT != relay_state) {
                    PT_WAIT_WHILE(pt, mean < T_target);
                }
                RELAY_ON;
                //tft_drawCircle(ring_get_cnt(&tft_rms), f_to_tft(f), 3, ILI9340_RED);
            }
            // ���� �������� - ���������
            else {
                RELAY_OFF;
                //tft_drawCircle(ring_get_cnt(&tft_rms), f_to_tft(f), 3, ILI9340_GREEN);
            }
        }

        // ��������� ���������� �����������
        // ������� ������ �������
        diff = absfix4(f - f_last_switch);
        t32 = (HAL_GetTick() - time_last_switch);
        rate = float2fix4((fix2float4(diff) / (t32 / 1000.)) * 60. * 60.);
        time_last_switch = t32;
        f_last_switch = f;

    } // END WHILE(1)
    PT_END(pt);
} // timer thread

PT_THREAD(protothread_temperature(pt_t* pt))
{
    PT_BEGIN(pt);
    static u32 timestamp = 0;

    // ����� �� ����� ����� ��������
    PT_DELAY_MS(timestamp, MS_IN_SEC);

    array[0] = mean = f = get_temperature(&OneWire);
    f_max = f_min = f;

    t_ready_sem = true;

    timestamp = HAL_GetTick() + 1000 - 1;
    while (1) {

        if (HAL_GetTick() > timestamp) {
            timestamp = HAL_GetTick() + 1000 - 1;
            f = get_temperature(&OneWire);
        }
        PT_YIELD(pt);
    } // END WHILE(1)
    PT_END(pt);
} // timer thread
