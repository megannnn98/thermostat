
#include "string.h"


#if DEBUG
#include "stm32f4xx_hal.h"
#define PRINTF(...)                                                            \
    do {                                                                      \
        extern char buffer[];                                                 \
        extern UART_HandleTypeDef H_UART;                                     \
        sprintf(buffer, __VA_ARGS__);                                         \
        HAL_UART_Transmit(&H_UART, (uint8_t*)buffer, strlen(buffer), 0x1000); \
        while(USART_SR_TC != (H_UART.Instance->SR & USART_SR_TC));  \
    } while (0);
#else
#define PRINTF(...)
#endif