

#include <stdbool.h>
#include "common_types.h"

#define DS18B20_SKIP_ROM            0xcc
#define DS18B20_CONVERT_T           0x44
#define DS18B20_READ_SCRATCHPAD     0xbe

#define WAIT_UNTIL_CONVERTION_COMPLETE(OneWire) while (0 == OneWire_ReadBit(OneWire))


#define   DS_port       GPIOC
#define   DS_pin        3
#define   DS_pin_MASK   (1 << 3)

#define   _us /5 



#define   PORT_OUTPUT DS_port->DDR |=  DS_pin_MASK
#define   PORT_INPUT  DS_port->DDR &= ~DS_pin_MASK

#define   PIN_LOW	  DS_port->ODR &= ~DS_pin_MASK
#define   PIN_HIGH	  DS_port->ODR |=  DS_pin_MASK

#define   OUTPUT_HIGH do {DS_port->DDR |=  DS_pin_MASK; DS_port->ODR |=   DS_pin_MASK;}while(0);
#define   OUTPUT_LOW  do {DS_port->DDR |=  DS_pin_MASK; DS_port->ODR &=  ~DS_pin_MASK;}while(0);









/* OneWire commands */
#define ONEWIRE_CMD_RSCRATCHPAD			0xBE
#define ONEWIRE_CMD_WSCRATCHPAD			0x4E
#define ONEWIRE_CMD_CPYSCRATCHPAD		0x48
#define ONEWIRE_CMD_RECEEPROM			0xB8
#define ONEWIRE_CMD_RPWRSUPPLY			0xB4
#define ONEWIRE_CMD_SEARCHROM			0xF0
#define ONEWIRE_CMD_READROM				0x33
#define ONEWIRE_CMD_MATCHROM			0x55
#define ONEWIRE_CMD_SKIPROM				0xCC

#define _DS18B20_TIMER TIM11
#define _DS18B20_H_TIMER htim11
extern TIM_HandleTypeDef htim11;

//extern TIM_HandleTypeDef htim17;

#define osDelay HAL_Delay

typedef struct {
	GPIO_TypeDef* GPIOx;           /*!< GPIOx port to be used for I/O functions */
	uint16_t GPIO_Pin;             /*!< GPIO Pin to be used for I/O functions */
	uint8_t LastDiscrepancy;       /*!< Search private */
	uint8_t LastFamilyDiscrepancy; /*!< Search private */
	uint8_t LastDeviceFlag;        /*!< Search private */
	uint8_t ROM_NO[8];             /*!< 8-bytes address of last search device */
} OneWire_t;

void OneWire_Init(OneWire_t* OneWireStruct, GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);
uint8_t OneWire_Reset(OneWire_t* OneWireStruct);
uint8_t OneWire_ReadByte(OneWire_t* OneWireStruct);
void OneWire_WriteByte(OneWire_t* OneWireStruct, uint8_t byte);
void OneWire_WriteBit(OneWire_t* OneWireStruct, uint8_t bit);
uint8_t OneWire_ReadBit(OneWire_t* OneWireStruct);
uint8_t OneWire_Search(OneWire_t* OneWireStruct, uint8_t command);
void OneWire_ResetSearch(OneWire_t* OneWireStruct);
uint8_t OneWire_First(OneWire_t* OneWireStruct);
uint8_t OneWire_Next(OneWire_t* OneWireStruct);
void OneWire_GetFullROM(OneWire_t* OneWireStruct, uint8_t *firstIndex);
void OneWire_Select(OneWire_t* OneWireStruct, uint8_t* addr);
void OneWire_SelectWithPointer(OneWire_t* OneWireStruct, uint8_t* ROM);
uint8_t OneWire_CRC8(uint8_t* addr, uint8_t len);



//#include "cmsis_os.h"
//#include "onewire.h"
//#include "ds18b20Config.h"
#include <stdbool.h>

//###################################################################################
typedef struct
{
	uint8_t 	Address[8];
	float 		Temperature;
	bool			DataIsValid;	
	
}Ds18b20Sensor_t;
//###################################################################################

extern Ds18b20Sensor_t	ds18b20[1];

//###################################################################################
/* Every onewire chip has different ROM code, but all the same chips has same family code */
/* in case of DS18B20 this is 0x28 and this is first byte of ROM address */
#define DS18B20_FAMILY_CODE						0x28
#define DS18B20_CMD_ALARMSEARCH				0xEC

/* DS18B20 read temperature command */
#define DS18B20_CMD_CONVERTTEMP				0x44 	/* Convert temperature */
#define DS18B20_DECIMAL_STEPS_12BIT		0.0625
#define DS18B20_DECIMAL_STEPS_11BIT		0.125
#define DS18B20_DECIMAL_STEPS_10BIT		0.25
#define DS18B20_DECIMAL_STEPS_9BIT		0.5

/* Bits locations for resolution */
#define DS18B20_RESOLUTION_R1					6
#define DS18B20_RESOLUTION_R0					5

/* CRC enabled */
#ifdef DS18B20_USE_CRC	
#define DS18B20_DATA_LEN							9
#else
#define DS18B20_DATA_LEN							2
#endif

//###################################################################################
typedef enum {
	DS18B20_Resolution_9bits = 9,   /*!< DS18B20 9 bits resolution */
	DS18B20_Resolution_10bits = 10, /*!< DS18B20 10 bits resolution */
	DS18B20_Resolution_11bits = 11, /*!< DS18B20 11 bits resolution */
	DS18B20_Resolution_12bits = 12  /*!< DS18B20 12 bits resolution */
} DS18B20_Resolution_t;

//###################################################################################
//void			Ds18b20_Init(osPriority Priority);
bool			Ds18b20_ManualConvert(void);
//###################################################################################
uint8_t 	DS18B20_Start(OneWire_t* OneWireStruct, uint8_t* ROM);
void 			DS18B20_StartAll(OneWire_t* OneWireStruct);
bool		 	DS18B20_Read(OneWire_t* OneWireStruct, uint8_t* ROM, float* destination);
uint8_t 	DS18B20_GetResolution(OneWire_t* OneWireStruct, uint8_t* ROM);
uint8_t 	DS18B20_SetResolution(OneWire_t* OneWireStruct, uint8_t* ROM, DS18B20_Resolution_t resolution);
uint8_t 	DS18B20_Is(uint8_t* ROM);
uint8_t 	DS18B20_SetAlarmHighTemperature(OneWire_t* OneWireStruct, uint8_t* ROM, int8_t temp);
uint8_t 	DS18B20_SetAlarmLowTemperature(OneWire_t* OneWireStruct, uint8_t* ROM, int8_t temp);
uint8_t 	DS18B20_DisableAlarmTemperature(OneWire_t* OneWireStruct, uint8_t* ROM);
uint8_t 	DS18B20_AlarmSearch(OneWire_t* OneWireStruct);
uint8_t 	DS18B20_AllDone(OneWire_t* OneWireStruct);
//###################################################################################

void ONEWIRE_LOW(OneWire_t *gp);
void ONEWIRE_HIGH(OneWire_t *gp);
void ONEWIRE_INPUT(OneWire_t *gp);
void ONEWIRE_OUTPUT(OneWire_t *gp);
void ONEWIRE_DELAY_5();
void ONEWIRE_DELAY_10();
void ONEWIRE_DELAY_15();
void ONEWIRE_DELAY_20();
void ONEWIRE_DELAY_100();
void ONEWIRE_DELAY_60();
void ONEWIRE_DELAY_480();

// Delay in I * 5us
void _delay_x5_us(u16 i);
s8 ds_reset();
void ds_writeSlot(bool i);
u8 ds_readSlot (void);
void ds_writeByte (u8 byte);
u8 ds_readByte (void);
float convertTemperToFloat(u8* p);
fix4 get_temperature(OneWire_t* OneWire);


