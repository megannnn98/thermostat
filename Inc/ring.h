

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "common_types.h"

#define RING_SIZE 100

typedef struct {
    u16 ring_array[RING_SIZE];
    u16 head;
    u16 tail;
    u16 cnt;
} ring_t;

extern ring_t tft_rms, tft_means;

void ring_put_char(ring_t* buf, const u16 sym);
u16 ring_get_char(ring_t* buf);
u16 ring_peek_char(ring_t* buf, u16 shift);
u16 ring_get_head(ring_t* buf);
u16 ring_get_cnt(ring_t* buf);
void ring_readout(ring_t* buf, u8 p[]);
void ring_flush(ring_t* buf);
