/**
  ******************************************************************************
  * File Name          : main.h
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H
  /* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */
#include "stm32f4xx_hal.h"
/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define B1_Pin GPIO_PIN_13
#define B1_GPIO_Port GPIOC
#define LD2_Pin GPIO_PIN_5
#define LD2_GPIO_Port GPIOA
#define TFT_SCK_Pin GPIO_PIN_10
#define TFT_SCK_GPIO_Port GPIOB
#define START_Pin GPIO_PIN_13
#define START_GPIO_Port GPIOB
#define SELECT_Pin GPIO_PIN_14
#define SELECT_GPIO_Port GPIOB
#define TFT_MOSI_Pin GPIO_PIN_15
#define TFT_MOSI_GPIO_Port GPIOB
#define RELAY_Pin GPIO_PIN_8
#define RELAY_GPIO_Port GPIOC
#define TFT_CS_Pin GPIO_PIN_8
#define TFT_CS_GPIO_Port GPIOA
#define TFT_A0_Pin GPIO_PIN_9
#define TFT_A0_GPIO_Port GPIOA
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
#define _DS18B20_Pin GPIO_PIN_10
#define _DS18B20_GPIO_Port GPIOC
#define SWO_Pin GPIO_PIN_3
#define SWO_GPIO_Port GPIOB
#define TFT_RESET_Pin GPIO_PIN_4
#define TFT_RESET_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

typedef enum {
    sWAIT = 0,
    sRELAY_ON,
    sRELAY_OFF,
} relay_state_t;

#define MS_IN_SEC (1000UL)
#define MS_IN_MIN (60 * MS_IN_SEC)
#define MS_IN_HOUR (60 * MS_IN_MIN)

#define S_IN_MIN (60)
#define S_IN_HOUR (60 * S_IN_MIN)

#define H_UART huart2

enum {
    RELAY_WAKE_MSG = 1,
    SELECT_PUSHED_MSG,
    START_PUSHED_MSG,
    INFO_PUSHED_MSG,
    TFT_WAKE_MSG,
};

//#define PRINT(...)                                                            \
//    do {                                                                      \
//        extern char buffer[];                                                 \
//        extern UART_HandleTypeDef H_UART;                                     \
//        sprintf(buffer, __VA_ARGS__);                                         \
//        HAL_UART_Transmit(&H_UART, (uint8_t*)buffer, strlen(buffer), 0x1000); \
//    } while (0);

#define RELAY_ON                           \
    do {                                   \
        HAL_GPIO_WritePin(RELAY_GPIO_Port, RELAY_Pin, GPIO_PIN_SET); \
        relay_state = sRELAY_ON;           \
    } while (0)
#define RELAY_OFF                         \
    do {                                  \
        HAL_GPIO_WritePin(RELAY_GPIO_Port, RELAY_Pin, GPIO_PIN_RESET); \
        relay_state = sRELAY_OFF;         \
    } while (0)

#define BUTTON_NOT_PUSHED HAL_GPIO_ReadPin(B1_GPIO_Port, B1_Pin)
#define BUTTON_PUSHED (!BUTTON_NOT_PUSHED)
      
#define SELECT_NOT_PUSHED HAL_GPIO_ReadPin(SELECT_GPIO_Port, SELECT_Pin)
#define SELECT_PUSHED (!SELECT_NOT_PUSHED)

#define START_NOT_PUSHED HAL_GPIO_ReadPin(START_GPIO_Port, START_Pin)
#define START_PUSHED (!START_NOT_PUSHED)
      
#define FLASH_USER_T_TARGET (0x08060000)
#define FLASH_USER_FREQ     (FLASH_USER_T_TARGET + 2)

      
typedef struct {
    const char* name;
    const char* string;
    char  state;
} menu_t;

enum {
    PARENT  = 0,
    T_MAX,
    T_MIN,
    MAX_TIME,
    TMEAN,
    SIGMA,
    TARGET,
    RATE,
    FREQ,
    MENU_MAX
};

enum {
SELECT_BTN  = 0,
START_BTN, 
INFO_BTN, 
};
      
      
/* USER CODE END Private defines */

void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)

/**
  * @}
  */ 

/**
  * @}
*/ 

#endif /* __MAIN_H */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
